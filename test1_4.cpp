#include<stdio.h>
#include<stdlib.h>
main(){
   printf("Prosty kalkulator (sumuje dwie liczby float)\n");
   printf("         y = a + b\n");
   printf("----------------------------------------------\n");

   float x1, x2, suma, roznica, iloczyn, iloraz;

   printf("Podaj liczbe a : ");
   scanf("%f", &x1);
   printf("Podaj liczbe b : ");
   scanf("%f", &x2);


   suma     = x1 + x2;
   roznica  = x1 - x2;
   iloczyn  = x1 * x2;

   printf("Wynik %5.2f + %f = %.2f \n", x1, x2, suma);
   printf("Wynik %.2f - %f = %.2f \n", x1, x2, roznica);
   printf("Wynik %.2f * %f = %.2f \n", x1, x2, iloczyn);
   printf("Wynik %.2f / %f = %.2f \n", x1, x2, x1/x2);
   printf("\n\n");
   system("PAUSE");
}















/*
   printf("Podaj liczbe a : ");
   scanf("%f", &x1);
   printf("Podaj liczbe b : ");
   scanf("%f", &x2);
*/
