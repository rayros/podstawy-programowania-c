#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZEX 10
#define SIZEY 10
#define REAPET 5
int rnd(int min, int max);
int fillTable2D(int table[][SIZEY]);
int dynamic(int table[][SIZEY]);
int zachlanna(int table[][SIZEY]);
int zachlannaRecurenc(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY);
int polaczenia(int table[][SIZEY], int indexX, int indexY, int tablicapolaczen[][SIZEY]);
int printTable(int table[][SIZEY]);
int main (int argc, char *argv[])
{
	int table[SIZEX][SIZEY];
	int i;
	clock_t start,stop;
	rndInit();
	for (i = 0; i < REAPET; i += 1)
	{
	fillTable2D(table);
	int zachlanna;
	printf("Zachlanna:");
	start=clock();
		zachlanna = findWayZ(table, SIZEX, SIZEY);
	stop=clock();
	printf(" CZAS %f s \n",(double)(stop-start)/CLOCKS_PER_SEC);
	printf("Koszt = %i \n", zachlanna );
	int dynamiczna;
	printf("Dynamiczna:");
	start=clock();
		dynamiczna = dynamic(table);	
	stop=clock();
	printf(" CZAS %f s \n",(double)(stop-start)/CLOCKS_PER_SEC);
	printf("Koszt = %i \n", dynamiczna );
	int zachlanna2;
	printf("Zachlanna rekurencyjna:");
	start=clock();
		zachlanna2 = zachlannaRecurenc(table, SIZEX, SIZEY, 0, 0);
	stop=clock();
	printf(" CZAS %f s \n",(double)(stop-start)/CLOCKS_PER_SEC);
	printf("Koszt = %i \n", zachlanna2 );
	}
	return 0;
	
}
int findWayZ(int t[SIZEX][SIZEY], int sX, int sY){
   int i = 0, j = 0;
   int cost = t[0][0];
   while((i != sX -1) || (j != sY-1)){
      if(i < sX-1 && j < sY-1){
         if(t[i+1][j] < t[i][j+1]){
            if(t[i+1][j+1] <= t[i+1][j] || (i+1 == SIZEX-1 && j+1 == SIZEY-1)){
                cost += t[++i][++j];
            } else {
                cost += t[++i][j];
            }
         }
         else if(t[i+1][j+1] <= t[i][j+1]){
            cost += t[++i][++j];
            } else {
                cost += t[i][++j];
            }
      }
      else if(i == sX-1){
         cost += t[i][++j];
      }
      else{
         cost += t[++i][j];
      }
   }
   return cost;
}

int zachlannaRecurenc(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY){
	int c1, c2, c3;
	int cost = t[pozX][pozY];
	if((pozX == sX-1) && (pozY == sY-1)){
		return cost;
	}
	if(pozX == sX-1){
		cost += zachlannaRecurenc(t, sX, sY, pozX,pozY+1);
		return cost;
	}
	else if(pozY == sY-1){
		cost += zachlannaRecurenc(t, sX, sY, pozX+1,pozY);
		return cost;
	}
	else{
		c1 = zachlannaRecurenc(t, sX, sY, pozX,pozY+1);
		c2 = zachlannaRecurenc(t, sX, sY, pozX+1,pozY);
		c3 = zachlannaRecurenc(t, sX, sY, pozX+1,pozY+1);
		if(c1 < c2){
			if(c3<=c1){
				cost += c3;
			} else {
				cost += c1;
			}
		}
		else{
			if(c3<=c2){
				cost += c3;
			} else {
				cost += c2;
			}
		}
	}
	return cost;
}
int dynamic(int table[][SIZEY])
{
	int ct[SIZEX][SIZEY];
	int i,j;
	for (i = SIZEY-1; i >= 0; i -= 1)
	{
		for (j = SIZEX-1; j >= 0; j -= 1)
		{
			polaczenia(table,i,j,ct);
		}
	}
	return ct[0][0];
}
int printTable(int table[][SIZEY]){
	int i,j;
	for (i = 0; i < SIZEX; i += 1)
	{
		for (j = 0; j < SIZEY; j += 1)
		{
			printf("%i ",table[i][j]);
		}
		printf("\n");
	}
}
int polaczenia(int table[][SIZEY], int indexX, int indexY, int tablicapolaczen[][SIZEY])
{
	int a,b,c,d,cost=0;
	if(indexY < SIZEY-1){
		a = tablicapolaczen[indexX][indexY+1]+table[indexX][indexY];
		if (cost)
		{
			if (cost > a)
			{
				cost = a;
			}
		}else{
			cost = a;
		}
	}
	if(indexX < SIZEX-1){
		b = tablicapolaczen[indexX+1][indexY]+table[indexX][indexY];
		if (cost)
		{
			if (cost > b)
			{
				cost = b;
			}
		}else{
			cost = b;
		}
	}
	if(indexX < SIZEX -1 && indexY < SIZEY-1){
		c = tablicapolaczen[indexX+1][indexY+1]+table[indexX][indexY];
		if (cost)
		{
			if (cost > c)
			{
				cost = c;
			}
		}else{
			cost = c;
		}
	}
	if(indexX == SIZEX-1 && indexY == SIZEY-1){
		d = table[indexX][indexY];
		if (cost)
		{
			if (cost > d)
			{
				cost = d;
			}
		}else{
			cost = d;
		}
	}
	tablicapolaczen[indexX][indexY] = cost;
	return 0;
}
int fillTable2D(int table[][SIZEY])
{
	int i,j;
	for (i = 0; i < SIZEX; i += 1)
	{
		for (j = 0; j < SIZEY; j += 1)
		{
			table[i][j] = rnd(0,9);
		}
	}	
	return 0;
}
int rndInit()
{
	srand(time(NULL));
}	
int rnd(int min, int max)
{
	return rand() %(max - min + 1) + min;
}

