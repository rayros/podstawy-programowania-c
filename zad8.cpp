#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char const* argv[])
{
	int a,b,c;
	float delta;
	printf("Podaj wartość a: ");
	scanf("%i",&a);
	printf("Podaj wartość b: ");
	scanf("%i",&b);
	printf("Podaj wartość c: ");
	scanf("%i",&c);
	printf("a: %i, b: %i, c: %i\n",a,b,c);
	delta = (b*b) - (4*a*c);
	
	if ( delta > 0 )
	{
		float x1 = (-b - sqrt(delta))/2*a;
		float x2 = (-b + sqrt(delta))/2*a;
		printf("Rozwiązanie równania x1 = %f, x2 = %f",x1,x2);
	}else {
		printf("%f",delta);
	}
	return 0;
}


