/*
	Fibo
	Liczenie ciągu Fibonacciego.
	Wersja: 6  // Zmiana podejścia.

	Paweł Łaski (c) 2013

	gcc -o fibo5 fibo5.c -std=c99 -march='nazwa_procesora' -O3 -fomit-frame-pointer 

	Udostępnione tylko do celów edukacyjnych.
	
	Wyraz nr. 100 000 liczy ~ X sekund po kompilacji z podanymi flagami.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>

/* Deklaracja typu bool */
typedef enum {TRUE = 1, FALSE = 0} bool;
/* Deklaracja typu struktury Big, który zawierać będzie bardzo długią liczbę oraz długość tablicy */
typedef struct Big {
	uint64_t *array;
	size_t size;
}** Big;

void initBig( Big *big , ...);
int main ()
{
	uint64_t liczba = 123456789;
	liczba = (liczba)/10;
	printf( "%llu\n",liczba );	
	Big a;
	initBig( a );	
	if (*a == NULL)
	{
		printf( "OK" );
	}
	return 0;
}
void initBig( Big *big ){
	/* Nadaje wartość NULL wskaźnikowi Big */
	*big = (struct Big**) malloc(sizeof(struct Big*));	
	**big = NULL;
}
