#include <server.h>
#include <curl/curl.h>
#include <string.h>
#include <stdlib.h>

static size_t write_data( void *ptr, size_t size, size_t nmemb, void *stream )
{
	int written = fwrite( ptr, size, nmemb, (FILE *)stream );
	return written;
}
char *join(const char* s1, const char* s2 )
{
	char* result = ( char* ) malloc( strlen( s1 ) + strlen( s2 ) + 1 );
	if( result )
	{
		strcpy(result, s1);
		strcat(result, s2);
	}
	return result;
}
int init( Site *st ){
	st->referer = NULL;
	return 0;
}
int get_page(Site *st, char *url, char* post)
{
	#ifdef DEBUG
	printf( "Try connect with %s\n",url );
	#endif
	CURL *curl_handle;
	//CURLcode res;
	static const char *headerfilename = "head.out";
	FILE *headerfile;
	static const char *bodyfilename = "body.out";
	FILE *bodyfile;
	curl_global_init(CURL_GLOBAL_ALL);

	st->url = url;

	/* init the curl session */ 
	curl_handle = curl_easy_init();

	/* set URL to get */ 
	curl_easy_setopt(curl_handle, CURLOPT_URL, st->url);
	#ifdef DEBUG
	curl_easy_setopt(curl_handle,  CURLOPT_VERBOSE, 1); 
	#endif
	//curl_easy_setopt(curl_handle, CURLOPT_HEADER, 1); 
	//curl_easy_setopt(curl_handle,  CURLOPT_RETURNTRANSFER, 1);
	if (st->referer != NULL)
	{
		curl_easy_setopt(curl_handle,CURLOPT_REFERER,st->referer); 
	}
	st->referer = join("http://",st->url);
	curl_easy_setopt(curl_handle,CURLOPT_AUTOREFERER,1); 
	
	//printf( "%s",st->referer );
	curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Ubuntu Chromium/24.0.1312.56 Chrome/24.0.1312.56 Safari/537.17");
	if (post != "")
	{
		curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, post);	
	}
	
	/* no progress meter please */ 
	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);	
	/* send all data to this function  */ 
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

	/* open the files */ 
	headerfile = fopen(headerfilename,"w");
	if (headerfile == NULL) {
		curl_easy_cleanup(curl_handle);
		return -1;
	}
	bodyfile = fopen(bodyfilename,"w");
	if (bodyfile == NULL) {
		curl_easy_cleanup(curl_handle);
		return -1;
	}
	/* we want the headers to this file handle */ 
	curl_easy_setopt(curl_handle, CURLOPT_COOKIEJAR, "cookie.out"); 
	curl_easy_setopt(curl_handle, CURLOPT_COOKIEFILE, "cookie.out");
	curl_easy_setopt(curl_handle, CURLOPT_WRITEHEADER, headerfile);
	curl_easy_setopt(curl_handle,  CURLOPT_WRITEDATA, bodyfile);
	
	/*
	* Notice here that if you want the actual data sent anywhere else but
	* stdout, you should consider using the CURLOPT_WRITEDATA option.  */ 

	/* get it! */ 
	curl_easy_perform(curl_handle);
	
	/* close the header file */ 
	fclose(headerfile);
	fclose(bodyfile);
	/* cleanup curl stuff */ 
	curl_easy_cleanup(curl_handle);
	return 0;
}

