#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "sleep.h"

int rndInit()
{
	srand( time( NULL ) );
	return 0;
}
int rnd( int min, int max )
{
    int tmp;
    if (max>=min)
        max-= min;
    else
    {
        tmp= min - max;
        min= max;
        max= tmp;
    }
    return max ? (rand() % max + min) : min;
}

int rndSleep(int min, int max){
	int a = rnd(min, max);
	sleep(a);
	return a;
}

