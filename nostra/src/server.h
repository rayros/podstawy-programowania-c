#ifndef H_SERVER
#define H_SERVER
typedef struct {
	char *url;
	char *referer;
} Site;
int init(Site *st);
int get_page(Site *st, char* url, char* post);
char *join( const char* s1, const char* s2 );
#endif
