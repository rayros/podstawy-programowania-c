#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define REPEAT 2000000
#define SIZEX 5
#define SIZEY 6

// Prototyp funkcji
void fillTab2D(int t[SIZEX][SIZEY], int sX, int sY); //wypelnia tablice
void printTab2D(int t[SIZEX][SIZEY], int sX, int sY); //wypisuje tablice

int findWayZ(int t[SIZEX][SIZEY], int sX, int sY); // zachlanna
int findWayR(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY); // zachlanna rekurencyjna
int findWayD(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY, int cost); // dokladna programowanie dynamiczne

int main(){
   // Inicjacja generatora liczb losowych
   srand((unsigned) time(NULL));

	clock_t start,stop;

   printf("Operowanie na tablicach 2D\n");
   printf("--------------------------\n");
	printf("\n");

   int tab[SIZEX][SIZEY];

   printf("Wypelniam losowa tablice... ");
   start=clock();
   fillTab2D(tab, SIZEX, SIZEY);
   stop=clock();
   printf(" zrobione w %f s \n",(float)(stop-start)/CLK_TCK);

   printf("Wypisuje tablice...\n ");
   printTab2D(tab, SIZEX, SIZEY);

    int kosztZ;
    printf("Szukam drogi metoda zachlanna ... ");
    start=clock();
    kosztZ = findWayZ(tab, SIZEX, SIZEY);
    stop=clock();
    printf(" zrobione w %f s \n",(float)(stop-start)/CLK_TCK);
    printf("Znaleziona droga ma koszt = %i \n", kosztZ );

    int kosztR;
    printf("Szukam drogi metoda zachlanna rekurencyjna : ");
    start=clock();
    kosztR = findWayR(tab, SIZEX, SIZEY, 0, 0);
    stop=clock();
    printf(" zrobione w %f s \n",(float)(stop-start)/CLK_TCK);
    printf("Znaleziona droga ma koszt = %i \n", kosztR );

    int kosztD;
    printf("Szukam drogi metoda dokladna (programowanie dynamiczne) ... ");
    start=clock();
    kosztD = findWayD(tab, SIZEX, SIZEY, SIZEX, SIZEY, tab[SIZEX-1][SIZEY-1]);
    stop=clock();
    printf(" zrobione w %f s \n",(float)(stop-start)/CLK_TCK);
    printf("Znaleziona droga ma koszt = %i \n", kosztD );


	printf("\n");
   system("PAUSE");
}

void fillTab2D(int t[SIZEX][SIZEY], int sX, int sY){
   int i, j;
   for(i = 0; i < sX; i++){
      for(j = 0; j < sY; j++){
         t[i][j] = rand() % 10;
      }
   }
}

void printTab2D(int t[SIZEX][SIZEY], int sX, int sY){
   int i, j;
   printf("\n");
   for(i = 0; i < sX; i++){
      for(j = 0; j < sY; j++){
         printf("%i ",t[i][j]);
      }
      printf("\n");
   }
}

int findWayZ(int t[SIZEX][SIZEY], int sX, int sY){
   int i = 0, j = 0;
   int cost = t[0][0];

   while((i != sX -1) || (j != sY-1)){
      if(i < sX-1 && j < sY-1){
         if(t[i+1][j] < t[i][j+1]){
            if(t[i+1][j+1] <= t[i+1][j]){
                cost += t[++i][++j];
            } else {
                cost += t[++i][j];
            }
         }
         else if(t[i+1][j+1] <= t[i][j+1]){
            cost += t[++i][++j];
            } else {
                cost += t[i][++j];
            }
      }
      else if(i == sX-1){
         cost += t[i][++j];
      }
      else{
         cost += t[++i][j];
      }
   }
   return cost;
}

int findWayR(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY){
   int c1, c2, c3;

   int cost = t[pozX][pozY];


   if((pozX == sX-1) && (pozY == sY-1)){
      return cost;
   }

   if(pozX == sX-1){
      cost += findWayR(t, sX, sY, pozX,pozY+1);
      return cost;
   }
   else if(pozY == sY-1){
      cost += findWayR(t, sX, sY, pozX+1,pozY);
      return cost;
   }
   else{
      c1 = findWayR(t, sX, sY, pozX,pozY+1);
      c2 = findWayR(t, sX, sY, pozX+1,pozY);
      c3 = findWayR(t, sX, sY, pozX+1,pozY+1);

      if(c1 < c2){
            if(c3<=c1){
                cost += c3;
            } else {
                cost += c1;
            }
      }
      else{
        if(c3<=c2){
            cost += c3;
        } else {
            cost += c2;
        }
      }
   }
   return cost;
}

int findWayD(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY,int cost){
   int c1, c2, c3;

   if((pozX == 0) && (pozY == 0)){
      return cost;
   }

   if(pozX == 0){
      cost += findWayD(t, sX, sY, pozX,pozY-1,cost);
      return cost;
   }
   else if(pozY == 0){
      cost += findWayD(t, sX, sY, pozX-1,pozY,cost);
      return cost;
   }
   else{
      c1 = findWayD(t, sX, sY, pozX,pozY-1,cost)+cost;
      c2 = findWayD(t, sX, sY, pozX-1,pozY,cost)+cost;
      c3 = findWayD(t, sX, sY, pozX-1,pozY-1,cost)+cost;

      if(c1 < c2){
            if(c3<=c1){
                cost += c3;
            } else {
                cost += c1;
            }
      }
      else{
        if(c3<=c2){
            cost += c3;
        } else {
            cost += c2;
        }
      }
   }


  return cost;
}

/*
int findWayD(int t[SIZEX][SIZEY], int sX, int sY){
    int cost = t[sX-1][sY-1];
    int i = sX-1, j = sY-1;

    while((i != 0) || (j != 0)){
      if(i > 0 && j > 0){
         if(t[i-1][j] < t[i][j-1]){
            if(t[i-1][j-1] <= t[i-1][j]){
                cost += t[--i][--j];
                printf("U ");
            } else {
                cost += t[--i][j];
                printf("G ");
            }
         }
         else if(t[i-1][j-1] <= t[i][j-1]){
            cost += t[--i][--j];
            printf("U ");
            } else {
                cost += t[i][--j];
                printf("L ");
            }
      }
      else if(i == 0){
         cost += t[i][--j];
         printf("kL ");
      }
      else{
         cost += t[--i][j];
         printf("kG ");
      }
   }


   return cost;
} */
