/*
	Fibo
	Liczenie ciągu Fibonacciego.
	Wersja: 4  // Digit - Zmiana z uint32_t na unit64_t.

	Paweł Łaski (c) 2013

	gcc -o fibo fibo.c -pedantic -Wall -Wextra -std=c99

	Udostępnione tylko do celów edukacyjnych.
	
	Wyraz nr. 100 000 liczy ok. 1,5 minuty
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#define TRUE 1
#define FALSE 0
#define DIGITMAX 1000000000000000000
#define DIGIT uint64_t

typedef DIGIT Digit;

typedef struct {
	Digit digit;
} QueueRecord;

typedef struct List{
	QueueRecord *record;
	struct List *previous;
	struct List *next;
} List;

typedef struct {
	List *first;
	List *last;
} Queue;

typedef Queue Big;

int queueInit( Queue *queue );
int queueAddLast( Queue *queue, Digit digit );
int queueAddFirst( Queue *queue, Digit digit );
int queueRemove( Queue *queue );
int queueRemoveAll( Queue *queue );
int queueDisplay( Queue *queue );

int invertBig( Big *big );
int addLastDigitToBig( Big *big, Digit digit);
int addFirstDigitToBig( Big *big, Digit digit);
int freeBig(Big *big);
int printInvertBig( Big *big );
int printBig( Big *big );
int initBig(Big *big);
int additionBigs(Big *a, Big *b, Big *result);
int copyToBig(Big *a, Big *b);
int canAddDigits(Digit a, Digit b);
Digit addDigits(Digit a, Digit b);

int main()
{ 

	clock_t start,finish;

	Big a, b, c;
	int in, i;
	printf( "\tDo którego wyrazu ciągu?\n" );
	printf( "\tNumer wyrazu?: " );
	scanf( "%i", &in );
	start = clock();
	initBig( &a );
	initBig( &b );
	initBig( &c );
	addFirstDigitToBig( &a, 1);
	printInvertBig(&a);
	printf( "\n" );
	addFirstDigitToBig( &b, 1);
	printInvertBig(&b);
	printf( "\n" );
	additionBigs( &a, &b, &c);
	printInvertBig(&c);
	printf( "\n" );
	for (i = 0; i < in-3; i += 1)
	{
		copyToBig(&a, &b);
		copyToBig(&b, &c);
		additionBigs( &a, &b, &c );
		printInvertBig(&c);
		printf( "\n" );
	}
	//printInvertBig(&c);
	printf( "\n" );
	freeBig( &a );
	freeBig( &b );
	freeBig( &c );
	finish = clock();
	double duration = (double)(finish-start) / CLOCKS_PER_SEC;
	printf( "%f",duration );
	printf( "\n\tKONIEC!\n" );
	getchar();
	return 0;
}
int copyToBig(Big *copy, Big *toCopy )
{
	freeBig(copy);
	List *list = toCopy->last;
	while( list )
	{
		addFirstDigitToBig( copy , list->record->digit);
		list = list->previous;
	}
	return 0;
}
int additionBigs(Big *a, Big *b, Big *result)
{
	freeBig(result);
	initBig(result);
	Digit temp = 0;
	Digit sum;
	List *big1 = a->first;
	List *big2 = b->first;
	while (big1 != NULL && big2 != NULL)
	{
		if(canAddDigits(big1->record->digit,big2->record->digit) == TRUE)
		{
			sum = addDigits(big1->record->digit,big2->record->digit);
			if (canAddDigits(sum,temp))
			{
				sum = addDigits(sum,temp);
				addLastDigitToBig( result , sum);
				temp = 0;
			}else{
				sum = addDigits(sum,temp);
				addLastDigitToBig( result , sum);
				temp = 1;
			}
		}
		else
		{
			sum = addDigits(big1->record->digit,big2->record->digit);
			sum = addDigits(sum,temp);
			addLastDigitToBig( result , sum);
			temp = 1;			
		}
		#ifdef DEBUG
			printf( "%i + %i + %i = %i \n", big1->record->digit, big2->record->digit, temp, sum );
		#endif
		big1 = big1->next;
		big2 = big2->next;
	}
	while (big1 != NULL)
	{
		if(canAddDigits(big1->record->digit,temp) == TRUE)
		{
			sum = addDigits(big1->record->digit,temp);
			addLastDigitToBig( result , sum);
			temp = 0;
		}
		else
		{		
			sum = addDigits(big1->record->digit,temp);
			addLastDigitToBig( result , sum);
			temp = 1;
		}
		big1 = big1->next;
	}
	while (big2 != NULL)
	{
		if(canAddDigits(big2->record->digit,temp) == TRUE)
		{
			sum = addDigits(big2->record->digit,temp);
			addLastDigitToBig( result , sum);
			temp = 0;
		}
		else
		{
			
			sum = addDigits(big1->record->digit,temp);
			addLastDigitToBig( result , sum);
			temp = 1;
		}
		big2 = big2->next;
	}
	if (temp >= 1)
	{
		addLastDigitToBig( result,temp);
	}
	return 0;
}
int initBig(Big *big)
{
	queueInit( big );
	return 0;
}
int freeBig(Big *big)
{
	queueRemoveAll(	big );
	return 0;
}
int invertBig(Big *big){
	Big result;
	queueInit( &result );
	List *a = big->first;
	while( a )
	{
		addFirstDigitToBig(&result,a->record->digit);
		a = a->next;
	}
	freeBig( big );
	*big = result;
	return 0;
}
int addFirstDigitToBig( Big *big, Digit digit){
	queueAddFirst(big, digit);
	return 0;
}
int addLastDigitToBig( Big *big, Digit digit)
{
	queueAddLast(big, digit);
	return 0;
}
int queueInit( Queue *queue )
{
	#ifdef DEBUG
		printf( "# <<<< Init Queue >>>>\n" );
	#endif
	queue->first = NULL;
	queue->last = NULL;
	return 0;
}
int queueAddFirst( Queue *queue, Digit digit )
{
	#ifdef DEBUG
		printf( "# queueAddLast: Adding new QueueRecord: digit - %i\n" ,digit );
	#endif
	QueueRecord *newRecord = ( QueueRecord* ) malloc( sizeof( QueueRecord ) );
	newRecord->digit = digit;
	List *newList = ( List* ) malloc( sizeof( List ) );
	newList->record = newRecord;
	newList->next = NULL;
	newList->previous = NULL;
	if( queue->first == NULL && queue->last == NULL )
	{
		queue->first = newList;
		queue->last = newList;
	}
	else
	{
		newList->next = queue->first;
		queue->first->previous = newList;
		queue->first = newList;
	}
	return 0;
}
int queueAddLast( Queue *queue, Digit digit )
{
	#ifdef DEBUG
		printf( "# queueAddLast: Adding new QueueRecord: digit - %i\n", digit );
	#endif
	QueueRecord *newRecord = ( QueueRecord* ) malloc( sizeof( QueueRecord ) );
	newRecord->digit = digit;
	List *newList = ( List* ) malloc( sizeof( List ) );
	newList->record = newRecord;
	newList->next = NULL;
	newList->previous = NULL;
	if( queue->first == NULL && queue->last == NULL )
	{
		queue->first = newList;
		queue->last = newList;
	}
	else
	{
		queue->last->next = newList;
		queue->last->next->previous = queue->last;
		queue->last = queue->last->next;
	}
	return 0;
}

int queueRemove( Queue *queue )
{
	#ifdef DEBUG
		printf( "# queueRemove: Removing record.\n" );
	#endif
	if( queue->first == NULL && queue->last == NULL )
	{
		return -1;
	}
	else if( queue->first->next == NULL )
	{
		free( queue->first->record );
		free( queue->first );
		queue->first = NULL;
		queue->last = NULL;
	}
	else
	{
		List *a = queue->first;
		queue->first = queue->first->next;
		free( a->record );
		free( a );
	}
	return 0;
}
int queueRemoveAll( Queue *queue )
{
	#ifdef DEBUG
		printf( "# queueRemoveAll: Removing records.\n" );
	#endif
	while( queueRemove( queue ) == 0 );
	return 0;
}
int queueDisplay( Queue *queue )
{
	#ifdef DEBUG
		printf( "# queueDisplay: Printing records.\n" );
	#endif
	List *a = queue->first;
	while( a )
	{
		printf( "# queueDisplay: Digit - %"PRIu64"\n", a->record->digit);
		a = a->next;
	}
	return 0;
}
int printBig( Big *big )
{
	#ifdef DEBUG
		printf( "# queueDisplay: Printing records.\n" );
	#endif
	List *a = big->first;
	while( a )
	{
		printf( "%"PRIu64"", a->record->digit);
		a = a->next;
	}
	return 0;
}
int printInvertBig( Big *big )
{
	#ifdef DEBUG
		printf( "# queueDisplay: Printing records.\n" );
	#endif
	List *a = big->last;
	int b = 0;
	while( a )
	{
		if(b==0){
			printf( "%"PRIu64"", a->record->digit);
			b = 1;
		}else{
			printf( "%018"PRIu64"",a->record->digit );
		}
		
		
		a = a->previous;
	}
	return 0;
}

int canAddDigits(Digit a, Digit b)
{
	if (DIGITMAX - 1 - a >= b)
	{
		#ifdef DEBUG
			printf( "%u >= %u ?",DIGITMAX - 1 - a ,b );
			printf( " TAK\n" );
		#endif
		return TRUE;
	}else {
		#ifdef DEBUG
			printf( "%u >= %u ?",DIGITMAX - 1 - a ,b );
			printf( " NIE\n" );
		#endif
		return FALSE;
	}
}
Digit addDigits(Digit a, Digit b)
{
	if (canAddDigits(a,b) == TRUE)
	{
		return a+b;
	}else {
		#ifdef DEBUG
			printf( "Too big.\n" );
		#endif
		return a+b-DIGITMAX;	
	}
}
