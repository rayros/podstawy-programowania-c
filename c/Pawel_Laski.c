#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
//#define DEBUG
#define SIZEX 14
#define SIZEY 14
#define REAPET 1
int rnd(int min, int max);
int fillTable2D(int table[][SIZEY]);
int dynamicMethod(int table[][SIZEY]);
int greedyMethod(int table[][SIZEY]);
int greedyMethodRecurenc(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY);
int connection(int table[][SIZEY], int indexX, int indexY, int tableConnection[][SIZEY]);
int printTable(int table[][SIZEY]);
int main (int argc, char *argv[])
{
	int table[SIZEX][SIZEY];
	int i;
	clock_t start,stop;
	rndInit();
	double greedy,dynamic,greedy2;
	for (i = 0; i < REAPET; i += 1)
	{
		fillTable2D(table);
		start=clock();
		greedyMethod(table);	
		stop=clock();
		greedy += (double)(stop-start)/CLOCKS_PER_SEC; 
		start=clock();
		dynamicMethod(table);	
		stop=clock();
		dynamic += (double)(stop-start)/CLOCKS_PER_SEC;
		start=clock();
		greedyMethodRecurenc(table, SIZEX, SIZEY, 0, 0);
		stop=clock();
		greedy2 += (double)(stop-start)/CLOCKS_PER_SEC;
	}
	printf("Wymiary tablicy: %i,%i\n",SIZEX,SIZEY);
	printf("Ilość powtórzeń: %i\n", REAPET);
	printf("Zachłanna: %f\n",greedy);
	printf("Zachlanna rekurencyjna: %f\n",greedy2);
	printf("Dynamiczna: %f\n",dynamic);
	return 0;
	
}

int greedyMethodRecurenc(int t[SIZEX][SIZEY], int sX, int sY, int pozX, int pozY){
	int c1, c2, c3;
	int cost = t[pozX][pozY];
	if((pozX == sX-1) && (pozY == sY-1)){
		return cost;
	}
	if(pozX == sX-1){
		cost += greedyMethodRecurenc(t, sX, sY, pozX,pozY+1);
		return cost;
	}
	else if(pozY == sY-1){
		cost += greedyMethodRecurenc(t, sX, sY, pozX+1,pozY);
		return cost;
	}
	else{
		c1 = greedyMethodRecurenc(t, sX, sY, pozX,pozY+1);
		c2 = greedyMethodRecurenc(t, sX, sY, pozX+1,pozY);
		c3 = greedyMethodRecurenc(t, sX, sY, pozX+1,pozY+1);

		if(c1 < c2){
			if(c3<=c1){
				cost += c3;
			} else {
				cost += c1;
			}
		}
		else{
			if(c3<=c2){
				cost += c3;
			} else {
				cost += c2;
			}
		}
	}
	return cost;
}
int greedyMethod(int table[][SIZEY]){
	int i = 0, j = 0;
	int temp = table[0][0];

	do{
		temp += table[i][j];
		if(i < SIZEX-1 && j < SIZEY-1){
			if(table[i+1][j] < table[i][j+1]){
				if( (table[i+1][j+1] <= table[i+1][j]) || (i+1 == SIZEX-1 && j+1 == SIZEY-1)){
					i += 1;
					j += 1;
				} 
				else
				{
					i+=1;
				}
			}
			else if(table[i+1][j+1] <= table[i][j+1])
			{
				i += 1;
				j += 1;
			} 
			else 
			{
				j += 1;
			}
		}
		else if(i == SIZEX-1)
		{
			j += 1;
		}
		else if(j == SIZEY-1)
		{
			i += 1;
		}
	}while((i != SIZEX-1) || (j != SIZEY-1));
	return temp;
}

int dynamicMethod(int table[][SIZEY])
{
	int ct[SIZEX][SIZEY];
	int i,j;
	for (i = SIZEY-1; i >= 0; i -= 1)
	{
		for (j = SIZEX-1; j >= 0; j -= 1)
		{
			connection(table,i,j,ct);
			//getchar();
		}
	}
	return ct[0][0];
}
int printTable(int table[][SIZEY]){
	int i,j;
	for (i = 0; i < SIZEX; i += 1)
	{
		for (j = 0; j < SIZEY; j += 1)
		{
			printf("%i ",table[i][j]);
		}
		printf("\n");
	}

}
int connection(int table[][SIZEY], int indexX, int indexY, int tableConnection[][SIZEY])
{
	int a,b,c,d,temp=0;
	//  |
	//  |
	//  |
	//  v
	if(indexY < SIZEY-1){
	
		a = tableConnection[indexX][indexY+1]+table[indexX][indexY];
		if (temp)
		{
			if (temp > a)
			{
				temp = a;
			}
		}else{
			temp = a;
		}
		#ifdef DEBUG
		printf("| [%i,%i] = [%i,%i](%i) + [%i,%i](%i) = (%i)\n",
			indexX,indexY,
			indexX,indexY,table[indexX][indexY],
			indexX,indexY+1,tableConnection[indexX][indexY+1]
			,a
			);
		#endif
	}
	//  ------>
	if(indexX < SIZEX-1){
		b = tableConnection[indexX+1][indexY]+table[indexX][indexY];
		if (temp)
		{
			if (temp > b)
			{
				temp = b;
			}
		}else{
			temp = b;
		}
		#ifdef DEBUG
		printf("- [%i,%i] = [%i,%i](%i) + [%i,%i](%i) = (%i)\n",
			indexX,indexY,
			indexX,indexY,table[indexX][indexY],
			indexX+1,indexY,tableConnection[indexX+1][indexY],
			b
		);
		#endif
	}
	//  \
	//   \
	//    \
	//     _|
	//
	if(indexX < SIZEX -1 && indexY < SIZEY-1){
		c = tableConnection[indexX+1][indexY+1]+table[indexX][indexY];
		if (temp)
		{
			if (temp > c)
			{
				temp = c;
			}
		}else{
			temp = c;
		}
		#ifdef DEBUG
		printf("\\ [%i,%i] = [%i,%i](%i) + [%i,%i](%i) = (%i)\n",
			indexX,indexY,
			indexX,indexY,table[indexX][indexY],
			indexX+1,indexY+1,tableConnection[indexX+1][indexY+1],
			c
		);
		#endif
	}
	if(indexX == SIZEX-1 && indexY == SIZEY-1){
		d = table[indexX][indexY];
		if (temp)
		{
			if (temp > d)
			{
				temp = d;
			}
		}else{
			temp = d;
		}
		#ifdef DEBUG
		printf("[%i,%i] = (%i)\n",
			indexX,indexY,
			table[indexX][indexY]
		);
		#endif
	}
	tableConnection[indexX][indexY] = temp;
	#ifdef DEBUG
	printf("WYNIK -> %i\n",temp);
	#endif
	return 0;
}
int fillTable2D(int table[][SIZEY])
{
	int i,j;
	for (i = 0; i < SIZEX; i += 1)
	{
		for (j = 0; j < SIZEY; j += 1)
		{
			table[i][j] = rnd(0,9);
		}
	}	
	return 0;
}
int rndInit()
{
	srand(time(NULL));
}	
int rnd(int min, int max)
{
	return rand() %(max - min + 1) + min;
}

