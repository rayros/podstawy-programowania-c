typedef struct {
	Point3D a;
	Point3D	b;
} Vector3D;
typedef struct {
	int x;
	int y;
	int z;
} Point3D;
Point3D createPoint3D(int x, int y, int z){
	Point3D a;
	a.x = x;
	a.y = y;
	a.z = z;
	return a;
}
