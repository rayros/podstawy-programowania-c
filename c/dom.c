#include <stdio.h>
#include <stdlib.h>
#include<math.h>

int main() {

    printf("------------------------------------------------\n");
    printf("Rozwiazywanie ukladu rownan metoda wyznacznikow \n");
    printf("------------------------------------------------\n");
    printf("\n");

    printf("Rownanie w postaci:\na1*x + b1*y = c1\na2*x + b2*y = c2\n\n");

    float a1, a2, b1, b2, c1, c2, x, y, W, Wx, Wy;

    //Dane do pierwszego r�wnania
    printf("Pierwsze r�wnanie\n");
    printf("Podaj a1: ");
    scanf("%f", &a1);
    printf("Podaj b1: ");
    scanf("%f", &b1);
    printf("Podaj c1: ");
    scanf("%f", &c1);
    //Dane do drugiego r�wnania
    printf("Drugie r�wnanie\n");
    printf("Podaj a2: ");
    scanf("%f", &a2);
    printf("Podaj b2: ");
    scanf("%f", &b2);
    printf("Podaj c2: ");
    scanf("%f", &c2);
 /*
 Czy chcesz wyswietlic jak wygladalaby twoja macierz?
 printf("Twoja macierz A:");
 printf("||\n||\n\n");
 */
    W = a1*b2 - a2*b1;
    Wx = c1*b2 - c2*b1;
    Wy = a1*c2 - a2*c1;

    x = Wx / W;
    y = Wy / W;

        if(W == 0){
            if (Wx == 0 || Wy == 0){
                printf("Nieskonczenie wiele rozwiazan.\n");
            }
            else{
                printf("Uklad sprzeczny.\n");
            }
        }
        else{
            printf("Uklad posiada rozwiazanie!\n");
            printf("x jest rowny: %f,\n y jest rowny: %f", x, y);
        }

	//return 0;
    printf("\n");
    system("PAUSE");
}
