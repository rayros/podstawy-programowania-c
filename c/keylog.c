//#include <wchar.h>
#include <stdio.h>

#include <stdlib.h>
// #define getc_unlocked _fgetc_nolock
int main ()
{
	FILE *fp; /* używamy metody wysokopoziomowej - musimydfdf
	mieć zatem identyfikator pliku, uwaga na gwiazdkę! */
	int c;
	char tekst[] = "Hello world";

	if ((fp=fopen("test.txt", "w+"))==NULL) {
		printf ("Nie mogę otworzyć pliku test.txt do zapisu!\n");
		exit(1);
	}
	fgetc_unlocked(fp);
	fprintf (fp, "%s", tekst); /* zapisz nasz łańcuch w pliku */
	while ( (c = fgetc(stdin)) != EOF) {
		fputc (c, stdout);
		fputc (c, fp);
	}
	fclose (fp); /* zamknij plik */
	return 0;
}
