#include <stdio.h>
#include <time.h>

#define SIZE 21
//Prototypy funkcji dla zadania 1
int rndInit();
int rnd(int min, int max);
int rndTable(float tA[], int size, int Vmin, int Vmax);
int printTab(float tA[], int size);
int cleverPrintTable(float t[], int size);
//Prototypy funkcji dla zadania 2
float findMinInTable(float tA[], int size);
float findMaxInTable(float tB[], int size);
//Prototypy funkcji dla zadania 3
float suma(float tab[], int size);
float srednia(float tA[], int size);
//Prototypy funkcji dla zadania 4
int normalizuj(float tA[], float tC[], int size, float max);
//Prototypy funkcji dla zadania 5
int punkt(float tC[], int size);
//Prototypy funkcji dla zadania 6
int fillciagIteracyjnie(float tD[], int size);
int fillciagRekurencyjnie(float tD[], int size, int i);
//Prototypy funkcji dla zadania 7
int insertSort(float table[],int tableWidth);
int findCiag(float tC[], int size, float *number, int *len);
//Prototypy funkcji dla zadania 8
int binarySearch (float array[], int size, float key, int strona);
//Prototypy funkcji dla zadania 9
int fillMatrix(float tA[],float tB[], float t2D[SIZE][SIZE], int size);
//Prototypy funkcji dla zadania 10
int cleverPrintTab2D(float t[][SIZE], int size);
int main (int argc, char *argv[])
{
	printf("Zadanie 1\n");
	//Inicjowanie srand
	
	rndInit();
	//Deklaracja tablic tA, tB
	float tA[SIZE],tB[SIZE];
	//Zakres wartości Vmin, Vmax
	int Vmin = 1, Vmax = 9;
	//Wypełaniamy tablicę tA
	rndTable(tA,SIZE,Vmin,Vmax);
	//Wypełaniamy tablicę tB
	rndTable(tB,SIZE,Vmin,Vmax);
	//Wypisanie tablicy tA w sprytny sposób
	cleverPrintTable(tA,SIZE);
	//Wypisanie tablicy tB w sprytny sposób
	cleverPrintTable(tB,SIZE);
	printf("Zadanie 2\n");
	//Deklaracja zmiennych max, min
	float max,min;
	//Max dla tablicy tA
	max = findMaxInTable(tA,SIZE);
	//Min dla tablicy tB
	min = findMinInTable(tB,SIZE);
	printf("Max z tablicy tA -> %f\n",max);
	printf("Min z tablicy tB -> %f\n",min);
	printf("Zadanie 3\n");
	printf("Średnia tablicy tA -> %f\n", srednia(tA,SIZE));
	printf("Średnia tablicy tB -> %f\n", srednia(tB,SIZE));
	printf("Zadanie 4\n");
	float tC[SIZE];
	normalizuj(tA, tC, SIZE, max);
	cleverPrintTable(tC,SIZE);
	printf("Zadanie 5\n");
	printf("Numer indexu punktu podziału i wynosi: %i\n", punkt(tC,SIZE));
	printf("Zadanie 6\n");
	float tD[SIZE];
	fillciagIteracyjnie(tD,SIZE);
	printf("Iteracyjnie: \n");
	cleverPrintTable(tD,SIZE);
	fillciagRekurencyjnie(tD,SIZE,0);
	printf("Rekurencyjnie\n");
	cleverPrintTable(tD,SIZE);
	printf("Zadanie 7\n");
	insertSort(tC,SIZE);
	cleverPrintTable(tC,SIZE);
	float number=0;
	int len=0;
	findCiag(tC,SIZE,&number,&len);
	printf("Najdłuższy fragment składa się z %i wartości %f\n", len, number);
	printf("Zadanie 8\n");
	printf("Lewa : %i Prawa: %i Różnica: %i\n",binarySearch(tC,SIZE,number,1),binarySearch(tC,SIZE,number,2)+binarySearch(tC,SIZE,number,2)-binarySearch(tC,SIZE,number,1), binarySearch(tC,SIZE,number,2)+binarySearch(tC,SIZE,number,2)-binarySearch(tC,SIZE,number,1)-binarySearch(tC,SIZE,number,1));
	
	printf("Zadanie 9\n");
	float t2D[SIZE][SIZE];
	printf("Wypełniam macierz t2D\n");
	fillMatrix(tA,tB,t2D,SIZE);
	printf("Drukujemy macierz t2D\n");
	cleverPrintTab2D(t2D,SIZE);
	return 0;
}

//Funkcje dla zadania 1
int rndInit()
{
	srand((unsigned)time(NULL));
	return 0;
}
int rnd(int min, int max)
{
	return rand() %(max - min + 1) + min;
}
int rndTable(float tA[], int size, int Vmin, int Vmax)
{
    int i;
    for(i = 0; i < size; i++){
        tA[i] =  rnd(Vmin,Vmax);
    }
    return 0;
}
int cleverPrintTable(float t[], int size)
{
    int i;
    if(size <= 20) printTab(t,size);
    else{
       for(i = 0; i < 10; i++){
           printf("%f ", t[i]);
       }
       printf("... ");
       for(i = size-10; i < size; i++){
           printf("%f ", t[i]);
       }
    }
    printf("\n");
    return 0;
}
int printTab(float tA[], int size)
{
    int i;
    for(i = 0; i < size; i++){
        printf("%f ", tA[i]);
    }
    return 0;
}
//Funkcje dla zadania 2
float findMinInTable(float tA[], int size)
{
   int i;
   float min = tA[0];
   for(i = 1; i < size; i++){
        if(min > tA[i]) min = tA[i];
   }
   return min;
}
float findMaxInTable(float tB[], int size)
{
   int i;
   float max = tB[0];
   for(i = 1; i < size; i++){
        if(max < tB[i]) max = tB[i];
   }
   return max;
}
//Funkcje dla zadania 3
float suma(float tab[], int size)
{
	int i;
	float sum = 0;   
	for (i = 0; i < size; i++)
	{        
		sum += tab[i];
	}
	return sum;
}
float srednia(float tA[], int size)
{
    float sum = suma(tA, size);
    float sred = sum / size;
    return sred;
} 
//Funkcje dla zadania 4
int normalizuj(float tA[], float tC[], int size, float max)
{
	int i;
	for (i = 0; i < size; i += 1)
	{
		//printf("%f %f %f\n",tA[i], max, tA[i]/max);
		tC[i] = tA[i]/max * 100;
	}
	return 0;
}
//Funkcje dla zadania 5
int punkt(float tC[], int size)
{
	float sum = suma(tC, size);
	float sumator = 0;
	int i = 0, a = 1;
	while(a == 1){
		if(sumator + tC[i] < sum/4){
			sumator += tC[i];
		}
		else {
			a = 0;
		}
		i += 1;
	}
	return i;
}
//Funkcje dla zadania 6
int fillciagIteracyjnie(float tD[], int size)
{
	int i;
	tD[0]=1;
	tD[1]=1;
	for (i = 2; i < size; i += 1)
	{
		tD[i] = 3*tD[i-1]-tD[i-2];
	}
	return 0;
}
int fillciagRekurencyjnie(float tD[], int size, int i)
{
	tD[0]=1;
	tD[1]=1;
	if (i == size)
	{
		return 0;
	}
	else
	{
		tD[i] = 3*tD[i-1]-tD[i-2];
		return fillciagRekurencyjnie(tD, size, i+1);
	}
}
//Funkcje dla zadania 7
int insertSort(float table[],int tableWidth)
{
	int i,j,index;
	float a;
	for ( i = 0; i < tableWidth; i += 1)
	{
		a = table[i];
		for ( j = i+1; j < tableWidth; j += 1)
		{
			if (table[j] > a)
			{
				a = table[j];
				index = j;
			}	
		}
			table[index]=table[i];
			table[i]=a;
	}
}
int findCiag(float tC[], int size, float *number, int *len)
{
	int temp=0,i;
	for (i = 0; i < size-1; i += 1)
	{
		if (tC[i] == tC[i+1])
		{
			temp +=1;
		}
		else 
		{
			if(temp > *len){
				*len = temp+1;
				*number = tC[i];
				temp = 0;
			}
			else
			{
				temp = 0;
			}
		}
	}
	return 0;
}
//Funkcje dla zadania 8
int binarySearch (float array[], int size, float key , int strona) 
{
	//strona lewa 1, strona prawa 2
	int min = 0;
	int max = size - 1;
	int i;
	while (max >= min) 
	{
		
		i = (min + max) / 2;
		//printf("MIN: %i , MAX: %i, I: %i ARRAY: %f\n",min,max,i,array[i]);
		if (key > array[i]) 
			max = i - 1;
		else if (key < array[i]) 
			min = i + 1;
		else{
			if (strona == 1)
			{
				while(i > 0){
					if (array[i-1] == array[i])
					{
						return i-1;
					}else{
						return i;
					}
					i -= 1;
				}
			}else if (strona == 2)
			{
				while(i < size){
					if (array[i+1] == array[i])
					{
						return i+1;
					}else{
						return i;
					}
					i += 1;
				}
			}
			
		}
			
	}
	return -1;
}
//Funkcje dla zadania 9
int fillMatrix(float tA[],float tB[], float t2D[SIZE][SIZE], int size)
{
	int i,j;
	for (i = 0; i < size; i += 1)
	{
		for (j = 0; j < size; j += 1)
		{
			if (i%2 == 0 && j%2 == 0)
			{
				t2D[i][j] = tA[i]+tB[j];
			}else
			{
				t2D[i][j] = tA[i] * tB[j];
			}
		}
	}
	return 0;
}
//Funkcje dla zadania 10
int cleverPrintTab2D(float t[][SIZE], int size)
{
    int i;
    if(size <= 20){
    	for (i = 0; i < size; i += 1)
    	{
    		printTab(t[i],size);
    	}
    } 
    else{
       for(i = 0; i < 10; i++){
           cleverPrintTable(t[i],size);
       }
       printf("...................\n");
       for(i = size-10; i < size; i++){
           cleverPrintTable(t[i],size);
       }
    }
    return 0;
}
