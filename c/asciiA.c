#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#define PLUS 0
#define MINUS 1
typedef struct {
	// Zbiory liczb
	// 0 = Natural - Naturalne
	// 1 = Total - Całkowite
	// 2 = Measurable - Wymierne
	// 3 = Irrational - Niewymierne
	// 4 = Real - Rzeczywiste
	// 5 = Prime - Pierwsze
	int set;
	struct SetOfNumbers *next;
} SetOfNumbers;
typedef struct {
	int sign;
	int value;
} Total;
typedef struct {
	// Sign - Znak
	// 0 - Plus - Dodawanie
	// 1 - Minus - Minus
	int sign;
	Total a;
	Total p;
	Total q;
} Fraction;
typedef struct {
	// Zbiory liczb
	// 0 = Natural - Naturalne
	// 1 = Total - Całkowite
	// 2 = Measurable - Wymierne
	// 3 = Irrational - Niewymierne
	// 4 = Real - Rzeczywiste
	// 5 = Prime - Pierwsze
	int set;
	
} Number;
typedef struct {
	int x;
	int y;
} Point2D;
typedef struct {
	Point2D a;
	Point2D b;
} Vector2D;
Point2D vectorCoordinates(Vector2D a);
Point2D createPoint2D(int x, int y);
Vector2D createVector2D(Point2D a,Point2D b);
bool isSqrt(int a); 
Total addTotal(Total a, Total b);
int main (int argc, char *argv[])
{
	printf("Hello!!!\n");
	Total a;
	a.sign = MINUS;
	a.value = 434;
	Total b;
	b.sign = PLUS;
	b.value = 34;
	Total x = addTotal(a,b);
	printf("%i  %i\n", x.sign, x.value);
	return 0;
}
Total addTotal(Total a, Total b){
	Total x;
	if (a.sign == b.sign)
	{
		x.sign = a.sign;
		x.value = a.value + b.value;
	}
	else
	{
		if (a.value >= b.value)
		{
			x.sign = a.sign;
			x.value = a.value - b.value;
		}
		else
		{
			x.sign = b.sign;
			x.value = b.value - a.value;
		}
	}
	return x;
}
Total subTotal(Total a, Total b){
	Total x;
	if (a.sign == b.sign)
	{
		if (a.value >= b.value)
		{
			x.sign = a.sign;
			x.value = a.value - b.value;
		}
		else
		{
			if (a.sign == PLUS)
			{
				x.sign = MINUS;
			}
			else
			{
				x.sign = PLUS;
			}
			x.value = b.value - a.value;
		}
	}
	else
	{
		x.sign = a.sign;
		x.value = a.value + b.value;
	}
	return x;
}
Fraction addFraction(Fraction a, Fraction b){

}
Point2D createPoint2D(int x, int y){
	Point2D a;
	a.x = x;
	a.y = y;
	return a;
}
Vector2D createVector2D(Point2D a,Point2D b){
	Vector2D v;
	v.a = a;
	v.b = b;
	return v;
}
Point2D vectorCoordinates(Vector2D v){
	Point2D p;
	p.x = v.b.x - v.a.x;
	p.y = v.b.y - v.a.y;
	return p;
}
bool isSqrt(int a){
	int b = (int) sqrt(a);
	if(a == b*b){ return true;}
	else{ return false;}
}
bool isNatural(int a){
	if (a >= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool isNaturalPlus(int a){
	if (a > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
