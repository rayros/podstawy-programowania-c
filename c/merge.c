//#include <iostream>
#include <stdio.h>
#include <time.h>
#define N 30
#include<stdlib.h>

void losujTab(int t[], int s){
    int i;
    for(i = 0; i < s; i++){
        t[i] =  rand() % 10;
    }
}
/* Scalanie dwoch posortowanych ciagow
   tab[pocz...sr] i tab[sr+1...kon] i
   wynik zapisuje w tab[pocz...kon] */
void merge(int t[],int tab[], int pocz, int sr, int kon)
{
  int i,j,q;
  for (i=pocz; i<=kon; i++) t[i]=tab[i];  // Skopiowanie danych do tablicy pomocniczej
  i=pocz; j=sr+1; q=pocz;                 // Ustawienie wskaników tablic
  while (i<=sr && j<=kon) {		  // Przenoszenie danych z sortowaniem ze zbiorów pomocniczych do tablicy głównej
    if (t[i]<t[j])
        tab[q++]=t[i++];
    else
        tab[q++]=t[j++];
  }
  while (i<=sr) tab[q++]=t[i++];	// Przeniesienie nie skopiowanych danych ze zbioru pierwszego w przypadku, gdy drugi zbiór się skończył
}

/* Procedura sortowania tab[pocz...kon] */
void mergesort(int t[], int tab[], int pocz, int kon)
{
  int sr;
  if (pocz<kon) {
    sr=(pocz+kon)/2;
    mergesort(t,tab,pocz, sr);    // Dzielenie lewej częci
    mergesort(t,tab,sr+1, kon);   // Dzielenie prawej częci
    merge(t,tab,pocz, sr, kon);   // Łšczenie częci lewej i prawej
  }
}

int main() {
	srand((unsigned) time(NULL));
	double duration;
	clock_t start,finish;
	start = clock();
	int i;
int tab[N];
int t[N];  // Tablica pomocnicza
losujTab(tab, N);
	printf("Zbior przed sortowaniem:\n");
	for (i=0; i<N; i++)
	   printf("%d ", tab[i]);

	mergesort(t,tab,0,N-1);

	printf("\nZbior po sortowaniu:\n");
	for (i=0; i<N; i++)
	   printf("%d ", tab[i]);
	finish = clock();
	/* Liczymy różnicę */
	duration = (double) (finish-start) / CLOCKS_PER_SEC;
	/* Drukujemy czas */
	printf( "%f\n", duration );

}
