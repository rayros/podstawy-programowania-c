#include <stdio.h>
#include <stdlib.h>


int silnia(int n);
float silniaF(int n);
double silniaD(int n);
int silniaR(int n);

void main(void){
	int a,b, result = 0;
	float resultF;
	double resultD;
	int resultR;

	printf("Program liczy silnie\n\nPodaj a:");
	scanf("%d",&a);

	result = silnia(a);
	printf("%d!=%d\n",a,result);

	resultF = silniaF(a);
	printf("%d!=%f\n",a,resultF);

	resultD = silniaD(a);
	printf("%d!=%Lf\n",a,resultD);

	resultR = silniaR(a);
	printf("%d!=%d\n",a,resultR);


	system("PAUSE");
}

int silnia(int n){
    int result = 1;
    int i;

    for(i = 1;i<=n ; i ++){
        result = result * i;
    }
    return result;
}

float silniaF(int n){
    float result = 1;
    int i;

    for(i = 1;i<=n ; i ++){
        result = result * i;
    }
    return result;
}

double silniaD(int n){
    double result = 1;
    int i;

    for(i = 1;i<=n ; i ++){
        result = result * i;
    }
    return result;
}

int silniaR(int n){
    int result = 1;
    int i;

    for(i = 1;i<=n ; i ++){
        result = silniaR(i-1) * i;
    }
    return result;
}
