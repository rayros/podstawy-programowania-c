#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int potegI(int b, int e);
int potegR(int b, int e);


void main(void){
    int b,e;
    int resultI, resultR;
    clock_t start,finish;
    clock_t start1,finish1;

    printf("Podaj liczbe i potege oddzielone spacja\n");
    scanf("%i %i",&b,&e);

    start = clock();
    resultI = potegI(b,e);
    printf("Wynik to %d\n",resultI);
    finish = clock();

	double duration = (double)(finish-start) / CLOCKS_PER_SEC;
	printf( "%f\n",duration );


    start1 = clock();
    resultR = potegR(b,e);
    printf("Wynik to %d\n",resultR);
	finish1 = clock();

    double duration1 = (double)(finish1-start1) / CLOCKS_PER_SEC;
	printf( "%f\n",duration1 );
}


int potegI(int b, int e){
	int result = b;
	int i;
        for(i = 1; i < e; i++){
            result = result * b;
        }
	return result;
}

int potegR(int b, int e){
    int result ;
    int i;
        if(e == 0){
            return 1;
        } else if((e%2)==0){
            result = potegR(b,e/2);
        } else {
            return potegR(b,e-1)* b;
            return result * result;
        }
}
