#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int task;
	int delay;
} Record;

typedef struct List{
	Record *record;
	List *next;
	List *previous;
} List;

typedef struct {
	List *first;
	List *last;
} Queue;

int queueInit(Queue *queue);
int queueAdd(Queue *queue, int task, int delay);
int queueRemove(Queue *queue);
int queueRemoveAll(Queue *queue);
int queueDisplay(Queue *queue);

int main (int argc, char const* argv[])
{
	Queue d;
	queueInit(&d);
	queueAdd(&d,50,10);
	queueAdd(&d,20,10);
	queueAdd(&d,30,10);
	queueAdd(&d,40,10);
	queueAdd(&d,60,10);
	queueAdd(&d,60,10);
	queueDisplay(&d);
	queueRemoveAll(&d);
	return 0;
}

int queueInit(Queue *queue){
	#ifdef DEBUG
	printf("# <<<< Init Queue >>>>\n");
	#endif
	queue->first = NULL;
	queue->last = NULL;
	return 0;
}
int queueAdd(Queue *queue, int task, int delay){
	#ifdef DEBUG
	printf("# queueAdd: Adding new record: task - %i , delay - %i\n",task,delay);
	#endif
	Record *new_record = (Record*)malloc(sizeof(Record));
	new_record->task = task;
	new_record->delay = delay;
	List *new_list= (List*)malloc(sizeof(List));
	new_list->record = new_record;
	new_list->next = NULL;
	new_list->previous = NULL;
	if(queue->first == NULL && queue->last == NULL){
		queue->first = new_list;
		queue->last = new_list;
	}else{
		queue->last->next = new_list;
		queue->last->next->previous = queue->last;
		queue->last = queue->last->next;
	}
	return 0;
}
int queueRemove(Queue *queue){
	#ifdef DEBUG
	printf("# queueRemove: Removing record\n");
	#endif
	if(queue->first == NULL && queue->last == NULL){
		return -1;
	}else if ( queue->first->next == NULL ) {
		free(queue->first->record);
		free(queue->first);
		queue->first = NULL;
		queue->last = NULL;
	}else{
		List *a = queue->first;
		queue->first = queue->first->next;
		free(a->record);
		free(a);
	}
	return 0;
}
int queueRemoveAll(Queue *queue){
	#ifdef DEBUG
	printf("# queueRemoveAll: Removing record\n");
	#endif
	while(queueRemove(queue)==0);
	return 0;
}
int queueDisplay(Queue *queue){
	#ifdef DEBUG
	printf("# queueDisplay: Printing record\n");
	#endif
	List *a = queue->first;
	while(a){
		printf("Task - %i, delay - %i \n", a->record->task, a->record->delay);
		a = a->next;
	}
	return 0;
}
