#include <stdio.h>
#include <stdlib.h>

typedef struct ArrayRecord {
	int integer; 
} ArrayRecord;

typedef struct Array {
	ArrayRecord *record;
	unsigned long int index;
	struct Array *previous;
	struct Array *next;
} Array;
int ArrayInit( Array *array );


int main ()
{
	Array a;
	ArrayInit( &a );
	return 0;
}
int arrayInit( Array *array ){
	#ifdef DEBUG
	printf( "# <<<< Init Array >>>>\n" );
	#endif
	array->record = NULL;
	array->previous = NULL;
	array->next = NULL;
	return 0;
}
int arrayAdd( Array *array, int integer ){
	#ifdef DEBUG
	printf( "# arrayAdd: Adding new ArrayRecord: integer - %i\n", integer );
	#endif
	ArrayRecord *newRecord = ( ArrayRecord* ) malloc( sizeof( ArrayRecord ) );
	newRecord->integer = integer;
	if( array->previous == NULL && array->next == NULL && array->record == NULL)
	{
		array->record = newRecord;
		array->index = 0;
	}
	else
	{
		queue->last->next = newList;
		queue->last->next->previous = queue->last;
		queue->last = queue->last->next;
	}
	
}
