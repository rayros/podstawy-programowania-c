/*Under Ctrl*/

#include <sys/time.h> /* timeval */
#include <sys/socket.h> /* select() */
 #include <stdio.h> /* printf() */
 #include <string.h> /* strlen() */
 
typedef unsigned int useconds_t;
 
/* attend pendant x microsend */
int usleep(useconds_t microseconds)
{
        if(microseconds)
        {
                const useconds_t one_second = 1000000;
                struct timeval tv_delay;
 
                tv_delay.tv_sec = microseconds / one_second;
                tv_delay.tv_usec = microseconds % one_second;
 
                return select(0,NULL,NULL,NULL,&tv_delay);
        }
 
        return 0;
}
/* attend pendant x milisecondes */
int Sleep(int miliseconds)
{
        return usleep(miliseconds * 1000);
}
int printDelay(int time, char* string){
	int interval = time/strlen(string);
	size_t i;
	for ( i = 0; i <= strlen(string); i += 1)
	{
		Sleep(interval);
		printf( "%c",string[i] );
		fflush(stdout);
	}
	return 0;
	
}
int main ()
{
	printf( "\n\t" );
	printDelay(2000,"This was a triumph!");
	Sleep(2000);
	printf( "\n\t" );
	printDelay(2000,"I'm making a note here:");
	printf( "\n\t" );
	printDelay(2000,"HUGE SUCCESS!!!");
	Sleep(2000);
	printf( "\n\n\t" );
	printDelay(2000,"It's hard to overstate");
	Sleep(200);
	printf( "\n\t" );
	printDelay(2000,"my satisfaction.");
	printf( "\n\n\t" );
	printDelay(4000,"'Still Alive' by Jonathan Coulton \n\n");
	return 0;
}
