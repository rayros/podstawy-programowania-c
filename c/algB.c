#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int generateTable(int table[], int tableWidth);
int printTable(int table[], int tableWidth);
int bubleSort(int table[],int tableWidth);
int bubleSort2(int table[],int tableWidth);
int bubleSort3(int table[],int tableWidth);
int insertSort();
int insertSort2();
int main ()
{	

	srand((unsigned)time(NULL));
	int i;
	for ( i = 1; i <= 10; i += 1)
	{
		int tableWidth = 10000 * i;
		int table[tableWidth];
		generateTable( table, tableWidth );
		bubleSort3(table,tableWidth);
	}
	

	//printTable(table,tableWidth);
	return 0;
}
int bubleSort3(int table[],int tableWidth)
{
	double duration;
	clock_t start,finish;
	int i,temp,p,pmin=0,pmax = tableWidth-2;
	start = clock();
	do
	{
		p = -1;
		for ( i = pmin; i <= pmax; i += 1)
		{
				if( table[i] > table[i+1] )
				{
					//printf( "%i > %i = Większy\n",table[i],table[i+1] );
					temp = table[i+1];		
					table[i+1]=table[i];
					table[i]=temp;
					p = i;
				}		
		}
		if(p < 0) break;
		pmax = p - 1;
		p = -1;
		for ( i = pmax; i >= pmin; i -= 1)
		{
				if( table[i] > table[i+1] )
				{
					//printf( "%i > %i = Większy\n",table[i],table[i+1] );
					temp = table[i+1];		
					table[i+1]=table[i];
					table[i]=temp;
					p = i;
				}		
		}
		pmin = p + 1;	
	}
	while( p >= 0);
	/* Zapisujemy końcowy czas */
	finish = clock();
	/* Liczymy różnicę */
	duration = (double) (finish-start) / CLOCKS_PER_SEC;
	/* Drukujemy czas */
	printf( "%f\n", duration );
	return 0;
}
int bubleSort2(int table[],int tableWidth)
{
	double duration;
	clock_t start,finish;
	int i,temp,p,pmin=0,pmax = tableWidth-1;
	start = clock();
	do
	{
		p = -1;
		for ( i = pmin; i < pmax; i += 1)
		{
			if( table[i] > table[i+1] )
			{
				//printf( "%i > %i = Większy\n",table[i],table[i+1] );
				temp = table[i+1];		
				table[i+1]=table[i];
				table[i]=temp;
				if(p < 0) pmin = i;
				p = i;
			}	
		}
		if(pmin) pmin--;
		pmax = p;	
	}
	while( p>= 0);
	/* Zapisujemy końcowy czas */
	finish = clock();
	/* Liczymy różnicę */
	duration = (double) (finish-start) / CLOCKS_PER_SEC;
	/* Drukujemy czas */
	printf( "%f\n", duration );
	return 0;
}
int insertSort(int table[],int tableWidth){
	double duration;
	clock_t start,finish;
	start = clock();
	int i,j,a,index;
	for ( i = 0; i < tableWidth; i += 1)
	{
		a = table[i];
		for ( j = i+1; j < tableWidth; j += 1)
		{
			if (table[j] < a)
			{
				a = table[j];
				index = j;
			}	
		}
		if( table[i]>a )
		{
			table[index]=table[i];
			table[i]=a;
		}
		
	}
		/* Zapisujemy końcowy czas */
	finish = clock();
	/* Liczymy różnicę */
	duration = (double) (finish-start) / CLOCKS_PER_SEC;
	/* Drukujemy czas */
	printf( "INSERTSort()  Czas obliczen: %f\n", duration );
	return 0;
}
int insertSort2(int table[],int tableWidth){
	double duration;
	clock_t start,finish;
	start = clock();
	int i,j,a,index;
	for ( i = 0; i < tableWidth; i += 1)
	{
		a = table[i];
		for ( j = i+1; j < tableWidth; j += 1)
		{
			if (table[j] < a)
			{
				a = table[j];
				index = j;
			}	
		}
			table[index]=table[i];
			table[i]=a;
		
		
	}
		/* Zapisujemy końcowy czas */
	finish = clock();
	/* Liczymy różnicę */
	duration = (double) (finish-start) / CLOCKS_PER_SEC;
	/* Drukujemy czas */
	printf( "%f\n", duration );
	return 0;
}
int bubleSort(int table[],int tableWidth)
{
	double duration;
	clock_t start,finish;
	int i,temp,a;
	start = clock();
	for ( a = 0; a < tableWidth; a += 1)
	{
		for ( i = 0; i < tableWidth; i += 1)
		{
			if(i+1 < tableWidth){
				if( table[i] > table[i+1] )
				{
					//printf( "%i > %i = Większy\n",table[i],table[i+1] );
					temp = table[i+1];		
					table[i+1]=table[i];
					table[i]=temp;
				}
			}
		}	
	}
	/* Zapisujemy końcowy czas */
	finish = clock();
	/* Liczymy różnicę */
	duration = (double) (finish-start) / CLOCKS_PER_SEC;
	/* Drukujemy czas */
	printf( "%f\n", duration );
	return 0;
}
int generateTable(int table[], int tableWidth)
{
	int i;
	for ( i = 0; i < tableWidth; i += 1)
	{
		table[i] = rand()%10;
	}
	return 0;
}
int printTable(int table[], int tableWidth)
{
	int i;
	printf( "TABLE: [ " );
	for ( i = 0; i < tableWidth; i += 1)
	{
		printf( "%i ",table[i] );
		
	}
	printf( "]\n" );
	return 0;
}
