/*
	Fibo
	Liczenie ciągu Fibonacciego.
	Wersja: 5  // Dużo zmian.

	Paweł Łaski (c) 2013

	gcc -o fibo5 fibo5.c -std=c99 -march='nazwa_procesora' -O3 -fomit-frame-pointer 

	Udostępnione tylko do celów edukacyjnych.

	Wyraz nr. 100 000 liczy ~ 10 sekund po kompilacji z podanymi flagami.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>
#define DIGITMAX 1000000000000000000

/* Deklaracja typu bool */
typedef enum {TRUE = 1, FALSE = 0} bool;
/* Deklaracja typu struktury Big, który zawierać będzie bardzo długią liczbę oraz długość tablicy */
typedef struct Big {
	uint64_t *array;
	uint64_t size;
} Big;

void initBig( Big **big );
void addBig( Big **big, uint64_t digit );
void freeBig( Big *a );
void printBig( Big *a );
void additionBigs( Big *a, Big *b, Big **result );
void copyToBig( Big **copy, Big *toCopy );
uint64_t addDigits( uint64_t a, uint64_t b );
bool canAddDigits( uint64_t a, uint64_t b );

int main ()
{
	/* Deklaracja zmiennych */
	int in, i;
	double duration;
	clock_t start,finish;
	Big *a, *b, *c; 
	/* Inicjowanie wskaźników Big */
	initBig( &a );
	initBig( &b );
	initBig( &c );
	/* Drukujemy */
	printf( "\n\tPodaj numer wyrazu ciagu.: " );
	/* Pobieramy numer wyrazu */
	if(scanf("%d", &in) == 1) {
		printf( "\tDziekuje. Proszę czekac...\n" );
	}else {
		printf( "\n\tFatalny problem. To chyba nie byla liczba. :(\n\n" );
		getchar();
		exit(0);
	}
	/* Startujemy z licznikiem czasu */
	start = clock();
	/* Sprawdzamy sobie wartość in */
	if (in == 1 || in == 2)
	{
		printf( "\tWynik:\n\n1" );
	}
	else if (in == 3)
	{
		printf( "\tWynik:\n\n2" );
	}
	else
	{
		/* Wstawiamy pierwsze 2 wyrazy */
		addBig(&a,1);
		addBig(&b,1);
		/* Sumujemy dwa poprzednie do Big c */
		additionBigs(a,b,&c);
		/* Pętla od wyrazu większego od 3 */
		for (i = 0; i < in-3; i += 1)
		{
			/* Kopiujemy Big b do Big a */
			copyToBig(&a, b);
			/* Kopiujemy Big c do Big b */
			copyToBig(&b, c);
			/* Sumujemy dwa poprzednie do Big c */
			additionBigs(a,b,&c);
			printBig(c);
			printf( "\n" );
			
		}
		printf( "\tWynik:\n\n" );
		/* Drukujemy liczbę Big */
		printBig(c);
	}
	/* Uwalniamy pamięć od Big'a */
	freeBig(a);
	freeBig(b);
	freeBig(c);
	/* Zapisujemy końcowy czas */
	finish = clock();
	/* Liczymy różnicę */
	duration = (double) (finish-start) / CLOCKS_PER_SEC;
	/* Drukujemy czas */
	printf( "\n\n\tCzas obliczen: %f", duration );
	printf( "\n\tKoniec programu.\n\n" );
	getchar();
	return 0;
}
void initBig( Big **big )
{
	/* Nadaje wartość NULL wskaźnikowi Big */
	*big = NULL;
}
void additionBigs(Big *a, Big *b, Big **result)
{
	/* Deklaracja zmiennych */
	uint64_t sum, i = 0, temp = 0;
	/* Uwalniamy pamięć od Big'a */
	freeBig( *result );
	/* Inicjowanie wskaźnika Big */
	initBig( result );
	/* 
	*  Algorytm dodawania.
	*  Start
	*/
	while( i <= a->size && i <= b->size )
	{
		if( canAddDigits( *(a->array + i), *(b->array + i) ) == TRUE )
		{
			sum = addDigits( *(a->array + i), *(b->array + i) );
			if ( canAddDigits( sum, temp ) )
			{
				sum = addDigits( sum, temp);
				addBig( result, sum);
				temp = 0;
			}else{
				sum = addDigits( sum, temp );
				addBig( result, sum );
				temp = 1;
			}
		}
		else
		{
			sum = addDigits( *(a->array + i), *(b->array + i) );
			sum = addDigits( sum, temp );
			addBig( result, sum);
			temp = 1;			
		}
		i++;
	}
	while( i <= a->size )
	{
		if( canAddDigits( *(a->array + i), temp ) == TRUE )
		{
			sum = addDigits( *(a->array + i), temp );
			addBig( result, sum );
			temp = 0;
		}
		else
		{		
			sum = addDigits( *(a->array + i), temp );
			addBig( result, sum );
			temp = 1;
		}
		i++;
	}
	while( i <= b->size )
	{
		if( canAddDigits( *(b->array + i), temp ) == TRUE )
		{
			sum = addDigits( *(b->array + i), temp );
			addBig( result, sum );
			temp = 0;
		}
		else
		{		
			sum = addDigits( *(b->array + i), temp );
			addBig( result, sum );
			temp = 1;
		}
		i++;
	}
	if( temp >= 1 )
	{
		addBig( result, temp );
	}
	/* 
	* Koniec 
	*/
}
/* Dodaje liczby z zakresu DIGITMAX - 1 */
uint64_t addDigits( uint64_t a, uint64_t b )
{
	
	if( canAddDigits( a, b ) == TRUE )
	{
		return a + b;
	}
	else
	{
		#ifdef DEBUG
			printf( "Too big.\n" );
		#endif
		return a + b - DIGITMAX;
	}
}
/* Sprawdza czy można dodać liczby w zakresie DIGITMAX - 1 */
bool canAddDigits( uint64_t a, uint64_t b )
{
	if( DIGITMAX - 1 - a >= b )
	{
		#ifdef DEBUG
			printf( "%u >= %u ?",DIGITMAX - 1 - a ,b );
			printf( " TAK\n" );
		#endif
		return TRUE;
	}
	else
	{
		#ifdef DEBUG
			printf( "%u >= %u ?",DIGITMAX - 1 - a ,b );
			printf( " NIE\n" );
		#endif
		return FALSE;
	}
}
/* Kopiuje Big toCopy do Big copy */
void copyToBig( Big **copy, Big *toCopy )
{
	freeBig( *copy );
	*copy = ( Big* ) malloc( sizeof( Big ) );
	(*copy)->array = ( uint64_t* ) malloc( ( toCopy->size + 1 ) * sizeof( uint64_t ) );
	memcpy( (*copy)->array, toCopy->array, ( toCopy->size + 1 ) * sizeof( uint64_t ) );
	(*copy)->size = toCopy->size;
}
/* Dodaje kolejene liczby do tablicy Big'a */
void addBig( Big **big, uint64_t digit )
{
	if ( *big == NULL )
	{
		*big = ( Big* ) malloc( sizeof(Big) );
		(*big)->array = ( uint64_t* ) malloc( sizeof( uint64_t ) ); 
		*( (*big)->array ) = digit;
		(*big)->size = 0;
	}
	else
	{
		(*big)->size += 1;
		uint64_t *b = ( uint64_t* ) realloc( (*big)->array, ( (*big)->size + 1 ) * sizeof( *b ) );
		if ( b == NULL )
		{
			printf( "Za mało pamięci. :(" );
			exit(0);
		}
		(*big)->array = b;
		*( (*big)->array + (*big)->size ) = digit;
	}
}
/* Drukuje Big'a */
void printBig( Big *a )
{
	char b = 0;
	uint64_t i = a->size;
	while( i > 0 )
	{
		if( b == 1 ){
			printf( "%018"PRIu64"", *( a->array + i ) );
		}else{
			printf( "%"PRIu64"", *( a->array + i ) );
			b = 1;
		}
		i--;
	}
	if(b==1){
			printf( "%018"PRIu64"", *( a->array ) );
		}else{
			printf( "%"PRIu64"", *( a->array ) );
			b = 1;
	}
}
/* Uwalnia pamięć od Big'a */
void freeBig( Big *a )
{
	if ( a != NULL )
	{
		free( a->array );
		free( a );
	}
}
