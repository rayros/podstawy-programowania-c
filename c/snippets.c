#include <stdlib.h>

typedef struct ${1:type}Array{
	${1:type} *array;
	size_t size;
} ${1:type}Array;

int ${1:type}ArrayInit( ${1:type}Array **array);
int ${1:type}ArrayAdd( ${1:type}Array **array, ${1:type} value );
${1:type} ${1:type}ArrayGet( ${1:type}Array **array, size_t index );
int ${1:type}ArrayFree( ${1:type}Array **array );

int ${1:type}ArrayInit( ${1:type}Array **array)
{
	*array = NULL;
	return 0;
}

int ${1:type}ArrayAdd( ${1:type}Array **array, ${1:type} value )
{
	if ( *array == NULL )
	{
		*array = ( ${1:type}Array* ) malloc( sizeof( ${1:type}Array ) );
		(*array)->array = ( ${1:type}* ) malloc( sizeof( ${1:type} ) ); 
		*( (*array)->array ) = value;
		(*array)->size = 0;
	}
	else
	{
		(*array)->size += 1;
		${1:type} *b = ( ${1:type}* ) realloc( (*array)->array, ( (*array)->size + 1 ) * sizeof( *b ) );
		if ( b == NULL )
		{
			printf( "Za mało pamięci. :(" );
			exit(0);
		}
		(*array)->array = b;
		*( (*array)->array + (*array)->size ) = value;
	}
	return 0;
}

${1:type} ${1:type}ArrayGet( ${1:type}Array **array, size_t index )
{
	if( *array != NULL )
	{
		return *((*array)->array + index);
	}
	else
	{
		return -1;
	}
	
}

int ${1:type}ArrayFree( ${1:type}Array **array )
{
	if ( *array != NULL )
	{
		free( (*array)->array );
		free( (*array) );
	}
	return 0;
}
$0
