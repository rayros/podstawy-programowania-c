#!/usr/bin/env python
def fibo(n):
        n = int(n)
        if n < 1: raise ValueError
        if n == 1: return 0
        if n == 2: return 1
        p = 0
        l = 1
        for i in xrange(2, n):
                p, l = l, p+l
        return l
       
import time
n = raw_input("Wyraz ciagu: ")
t1 = time.time()
x = fibo(n)
t2 = time.time()
print x
t3 = time.time()
print t2-t1, t3-t2
