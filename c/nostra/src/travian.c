#include "travian.h"
#include "sleep.h"



int login(Site *st, Data *data)
{
	xmlDocPtr doc;
	rndInit();
	init(st);
	get_page(st,"travian.pl/","");
	printf("Waiting 3-10\n");
	printf("Wait %i sec.\n",rndSleep(3,10));
	get_page(st,"ts1.travian.pl/","");
	doc = htmlInit("body.out");
		char post[200];
		snprintf(post,
			sizeof(post), 
			"%s%s",
			"name=zniwus&password=samsung1&lowRes=1&s1=Login&w=1024%3A600&login=",
			htmlGetValue(doc,"//form//input[@type = 'hidden' and @name = 'login']/@value"));
		char where[200];
		snprintf(where,
			sizeof(where), 
			"%s%s",
			"ts1.travian.pl/",
			htmlGetValue(doc,"//form/@action"));	
	xmlFreeDoc(doc);
	printf("Waiting 7-15\n");
	printf("Wait %i sec.\n",rndSleep(7,15));
	get_page(st,where,post);
	doc = htmlInit("body.out");
		/*<div class="error LTR">*/
		if(strcmp((char*)htmlGetValue(doc,"//div[@class = 'error LTR']"),"null")){
			printf( "Zły login lub hasło.\n" );
			xmlFreeDoc(doc);
			return -1;
		}else{
			printf( "Logowanie udane.\n" );
			getDorf1(data,doc);
		};
	xmlFreeDoc(doc);
	return 0;
}
int getDorf1(Data *data, xmlDocPtr doc)
{
	__getProduction(data,doc);
	for (int i = 1; i < 18; i += 1)
	{
		__addBuildDorf1(doc,data,i);
	}

	return 0;
}
int __addBuildDorf1(xmlDocPtr doc,Data *data, int i)
{
	Build *new = (Build*) malloc(sizeof(Build));
	char str[200];
	snprintf(str,
		sizeof(str), 
		"%s%i%s",
		"substring-before(//area[@href = 'build.php?id=",
		i,
		"']/@title, ' <')"
	);
	xmlChar *type = htmlGetValue(doc,str);
	if(!strcmp((char*)type , "Las")){new->type = WOOD;}
	else if(!strcmp((char*)type , "Kopalnia żelaza")){new->type = IRON;}
	else if(!strcmp((char*)type , "Pole zboża")){new->type = CEREAL;}
	else if(!strcmp((char*)type , "Kopalnia gliny")){new->type = CLAY;}
	else {new->type = UNDEFINED;}
	snprintf(str,
		sizeof(str), 
		"%s%i%s",
		"substring-after(//area[@href = 'build.php?id=",
		i,
		"']/@alt,'Poziom ')"	
	);
	new->id = i;
	new->level = atoi((char*)htmlGetValue(doc,str));
	//addTime(new,"7:8:7");
	printf( "%s => type: %i ", type, new->type );
	printf( "-> id: %i -> poziom: %i\n", i, new->level );
	return 0;
}
int __getProduction(Data *data, xmlDocPtr doc)
{
	char * pEnd;
	data->wood_per_hour = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r1']/@title,'||: ')"),&pEnd,10);
	data->clay_per_hour = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r2']/@title,'||: ')"),&pEnd,10);
	data->iron_per_hour = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r3']/@title,'||: ')"),&pEnd,10);
	data->cereal_per_hour = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r4']/@title,'||: ')"),&pEnd,10);
	data->amount_of_wood = strtol((char*)htmlGetValue(doc,
	"substring-before(//ul[@id = 'res']/li[@class = 'r1']/p/span,'/')"),&pEnd,10);
	data->amount_of_clay = strtol((char*)htmlGetValue(doc,
	"substring-before(//ul[@id = 'res']/li[@class = 'r2']/p/span,'/')"),&pEnd,10);
	data->amount_of_iron = strtol((char*)htmlGetValue(doc,
	"substring-before(//ul[@id = 'res']/li[@class = 'r3']/p/span,'/')"),&pEnd,10);
	data->amount_of_cereal = strtol((char*)htmlGetValue(doc,
	"substring-before(//ul[@id = 'res']/li[@class = 'r4']/p/span,'/')"),&pEnd,10);
	data->wood_storage = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r1']/p/span,'/')"),&pEnd,10);
	data->clay_storage = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r2']/p/span,'/')"),&pEnd,10);
	data->iron_storage = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r3']/p/span,'/')"),&pEnd,10);
	data->cereal_storage = strtol((char*)htmlGetValue(doc,
	"substring-after(//ul[@id = 'res']/li[@class = 'r4']/p/span,'/')"),&pEnd,10);
	//printf( "%ld" , strtol("23:33:44",&pEnd,10));
	return 0;
}
int __addTime(Build *build, char* time){
	char hh[3], mm[3], ss[3];
	unsigned long int s;
	sscanf( time, "%[^:]:%[^:]:%[^:]", hh, mm, ss );
	//printf( "%i : %i : %i\n", atoi( hh ), atoi( mm ), atoi( ss ) );
	s = atoi(hh)*3600+atoi(mm)*60+atoi(ss);
	#ifdef DEBUG
	printf( " #ADD TIME: '%s' => %s : %s : %s => %lu\n", time, hh, mm, ss, s );
	#endif
	return 0;
}
