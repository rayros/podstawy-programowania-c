#include <stdio.h>
#include <stdint.h>

#include <stdlib.h>
#include <string.h>
typedef enum {TRUE = 1, FALSE = 0} bool;
typedef struct uint64_tArray{
	uint64_t *array;
	size_t size;
} uint64_tArray;

int uint64_tArrayInit( uint64_tArray **array);
int uint64_tArrayAdd( uint64_tArray **array, uint64_t value );
uint64_t uint64_tArrayGet( uint64_tArray **array, size_t index );
int uint64_tArrayFree( uint64_tArray **array );
int uint64_tArrayJoin( uint64_tArray **arrayA, uint64_tArray **arrayB, uint64_tArray **arrayC);
int uint64_tArrayCopy( uint64_tArray **copy, uint64_tArray *toCopy );
bool canAddAdditions( uint64_t a, uint64_t b );


int print(uint64_tArray **array)
{
	uint64_t i;
	printf( "--- " );
	for ( i = 0; i <= (*array)->size; i += 1)
	{
		printf( "%llu ", *((*array)->array + i) );
	}
	printf( "---\n" );
	return 0;
}
int addition(uint64_tArray **arrayA, uint64_tArray **arrayB, uint64_tArray **arrayC)
{
	uint64_t a = *( (*arrayA)->array + (*arrayA)->size );
	uint64_t b = *( (*arrayB)->array + (*arrayB)->size );
	uint64_tArray *temp0;
	uint64_tArray *temp1;
	uint64_tArrayFree(arrayC);
	*arrayC = malloc(sizeof(uint64_tArray));
	if( *arrayC == NULL )
	{
		printf( "Za mało pamięci. :(" );
		exit(0);
	}
	if(canAddAdditions( a, b ) == TRUE )
	{
		a += b;
		uint64_tArrayInit(&temp0);
		uint64_tArrayAdd(&temp0, a);
		(*arrayC)->array = malloc( (((*arrayA)->size+1)+((*arrayB)->size))*sizeof(uint64_t));
		if( (*arrayC)->array == NULL )
		{
			printf( "Za mało pamięci. :(" );
			exit(0);
		}
		if((*arrayA)->size + (*arrayB)->size != 0)
		{
			memcpy( (*arrayC)->array, (*arrayA)->array, (*arrayA)->size * sizeof( uint64_t ) );
			memcpy( (*arrayC)->array + (*arrayA)->size, (*arrayB)->array, (*arrayB)->size * sizeof( uint64_t ) );	
		}
		memcpy( (*arrayC)->array + ((*arrayA)->size + (*arrayB)->size), (*temp0).array, ((*temp0).size + 1) * sizeof( uint64_t ) );
		(*arrayC)->size = (*arrayA)->size + (*arrayB)->size;
		uint64_tArrayFree(&temp0);
	}else
	{
		b = b - (UINT64_MAX - a);
		a = UINT64_MAX;
		uint64_tArrayInit(&temp0);
		uint64_tArrayAdd(&temp0, a);
		uint64_tArrayInit(&temp1);
		uint64_tArrayAdd(&temp1, b);
		(*arrayC)->array = malloc( (((*arrayA)->size+1)+((*arrayB)->size+1))*sizeof(uint64_t));
		if( (*arrayC)->array == NULL )
		{
			printf( "Za mało pamięci. :(" );
			exit(0);
		}
		if((*arrayA)->size + (*arrayB)->size != 0)
		{
			memcpy( (*arrayC)->array, (*arrayA)->array, (*arrayA)->size * sizeof( uint64_t ) );
			memcpy( (*arrayC)->array + (*arrayA)->size, (*arrayB)->array, (*arrayB)->size * sizeof( uint64_t ) );	
		}
		memcpy( (*arrayC)->array + ((*arrayA)->size + (*arrayB)->size), (*temp0).array, ((*temp0).size + 1) * sizeof( uint64_t ) );
		memcpy( (*arrayC)->array + ((*arrayA)->size + (*arrayB)->size + 1), (*temp1).array, ((*temp1).size + 1) * sizeof( uint64_t ) );
		(*arrayC)->size = (*arrayA)->size + (*arrayB)->size + 1;
		uint64_tArrayFree(&temp0);
		uint64_tArrayFree(&temp1);
	}
	return 0;
}


int main ()
{
	uint64_tArray *a,*b,*c;
	uint64_tArrayInit(&a);
	uint64_tArrayInit(&b);
	uint64_tArrayInit(&c);
	uint64_tArrayAdd(&a,1);
	print(&a);
	uint64_tArrayAdd(&b,1);
	print(&b);
	addition(&a,&b,&c);
	print(&c);
	//printf( "%llu\n", UINT64_MAX );
	
	int u;
	for ( u = 0; u < 200; u += 1)
	{
		uint64_tArrayCopy(&a,b);
		uint64_tArrayCopy(&b,c);
		addition(&a,&b,&c);
		
	}
	printf( "Koniec\n" );
	print(&c);
/*	for ( i = 0; i <= (*c).size; i += 1)*/
/*	{*/
/*		*/
/*		printf( "%llu: %llu\n", i, *((*c).array + i) );*/
/*		*/
/*	}*/
	uint64_tArrayFree(&a);
	uint64_tArrayFree(&b);
	uint64_tArrayFree(&c);
	return 0;
}

bool canAddAdditions( uint64_t a, uint64_t b )
{
	if( UINT64_MAX - a >= b )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
int uint64_tArrayInit( uint64_tArray **array )
{
	*array = NULL;
	return 0;
}

int uint64_tArrayAdd( uint64_tArray **array, uint64_t value )
{
	if ( *array == NULL )
	{
		*array = ( uint64_tArray* ) malloc( sizeof( uint64_tArray ) );
		if( *array == NULL )
		{
			printf( "Za malo pamieci :(\n" );
			exit(0);
		}
		(*array)->array = ( uint64_t* ) malloc( sizeof( uint64_t ) );
		if( (*array)->array == NULL)
		{
			printf( "Za mało pamięci. :(" );
			exit(0);
		} 
		*( (*array)->array ) = value;
		(*array)->size = 0;
	}
	else
	{
		(*array)->size += 1;
		uint64_t *b = ( uint64_t* ) realloc( (*array)->array, ( (*array)->size + 1 ) * sizeof( *b ) );
		if ( b == NULL )
		{
			free( (*array)->array );
			printf( "Za mało pamięci. :(" );
			exit(0);
		}
		(*array)->array = b;
		*( (*array)->array + (*array)->size ) = value;
	}
	return 0;
}
uint64_t uint64_tArrayGet( uint64_tArray **array, size_t index )
{
	if( *array != NULL )
	{
		return *((*array)->array + index);
	}
	else
	{
		return -1;
	}	
}
int uint64_tArrayFree( uint64_tArray **array )
{
	if ( *array != NULL )
	{
		free( (*array)->array );
		free( (*array) );
	}
	return 0;
}
int uint64_tArrayJoin( uint64_tArray **arrayA, uint64_tArray **arrayB, uint64_tArray **arrayC)
{
	uint64_tArrayFree(arrayC);
	*arrayC = malloc(sizeof(uint64_tArray));
	if( *arrayC == NULL )
	{
		printf( "Za mało pamięci. :(" );
		exit(0);
	}
	(*arrayC)->array = malloc( (((*arrayA)->size+1)+((*arrayB)->size+1))*sizeof(uint64_t));
	if( (*arrayC)->array == NULL )
	{
		printf( "Za mało pamięci. :(" );
		exit(0);
	}
	memcpy( (*arrayC)->array, (*arrayA)->array, ( (*arrayA)->size + 1 ) * sizeof( uint64_t ) );
	memcpy( (*arrayC)->array + ((*arrayA)->size + 1), (*arrayB)->array, ( (*arrayB)->size + 1 ) * sizeof( uint64_t ) );
	(*arrayC)->size = (*arrayA)->size + (*arrayB)->size + 1;
	return 0;
}
int uint64_tArrayCopy( uint64_tArray **copy, uint64_tArray *toCopy )
{
	uint64_tArrayFree( copy );
	*copy = malloc( sizeof( uint64_tArray ) );
	if( *copy == NULL )
	{
		printf( "Za mało pamięci. :(" );
		exit(0);
	}
	(*copy)->array = malloc( ( toCopy->size + 1 ) * sizeof( uint64_t ) );
	memcpy( (*copy)->array, toCopy->array, ( toCopy->size + 1 ) * sizeof( uint64_t ) );
	(*copy)->size = toCopy->size;
	return 0;
}

