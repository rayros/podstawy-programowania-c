#include <stdio.h>
#include <stdlib.h>
#define xmalloc(typ, ilosc) (typ*) _new((ilosc)*sizeof(typ))
#define xglebokoscDrzewa(root, glebia) glebokoscDrzewa((root), 0, &(glebia))
#define GOTOWKA 1
#define KARTA 0
FILE* xfopen(char* plik, char* tryb);

//Czesc A
typedef struct rachunek {
	char miejsce[100];
	int rok_zakupu;
	double kwota;
	int gotowka_karta;
} Rachunek;
//Czesc C
typedef struct element {
	Rachunek wartosc;
	struct element *nastepny;
} Element;
//Czesc D
typedef struct wezel {
	Rachunek wartosc;
	struct wezel *lewy;
	struct wezel *prawy;
} Wezel;
//Czesc A
Rachunek wypelnijRachunek();
int wypiszRachunek(Rachunek rachunek);
int zapisz_na_dysk(Rachunek rachunek);
Rachunek wczytaj_z_dysu();
//Czesc B
int podaj_ilosc_rachunkow();
Rachunek *stworzTabliceRachunkow(int ilosc);
int wypelnijTabliceRachunkow(Rachunek *tablica, int ilosc);
int wypiszTabliceRachunkow(Rachunek *tablica, int ilosc);
int sumaZakupu(Rachunek *tablica, int ilosc);
double sredniaZakupu(Rachunek *tablica, int ilosc);
int zakupyDokonaneKarta(Rachunek *tablica, int ilosc);
int zapiszTabliceRachunkowDoPliku(Rachunek *tablica, int ilosc);
//Czesc C
int policzIleRachunkowWLiscie(Element *lista);
Element *stworzElement(Rachunek wartosc, Element *nastepny);
double sredniaZakupuLista(Element *lista);
double sumaZakupuLista(Element *lista);
int wypiszListeRachunkow(Element *lista);
Element *stworzElement(Rachunek wartosc, Element *nastepny);
Element *dodajElementDoListy(Element *lista, Element *doDodania); //Append
Element *insertElementDoListy(Element *lista, Element *doDodania); //Insert
Element *wczytajPlikDoListy();
int kartaWLiscie(Element *lista);
int zapiszListyRachunkowDoPliku(Element *lista);
//Czesc D
int wiekszyRachunek(Rachunek pierwszy, Rachunek drugi);
Wezel *stworzWezel(Rachunek wartosc, Wezel *lewy, Wezel *prawy);
Wezel *dodajWezelDoDrzewa(Wezel *drzewo, Wezel *doDodania);
Wezel *wczytajPlikDoDrzewa();
int wypiszDrzewoRachunkow(Wezel *drzewo);
int glebokoscDrzewa(Wezel *root, int handler, int *glebia);
int sortujMalejacoDrzewoDoListy(Wezel *root, Element **lista);
int main()
{
	int ilosc, glebia;
	Rachunek *tablica;
	Element *lista,*wynik;
	Wezel *drzewo;
	wynik = NULL;
	ilosc = podaj_ilosc_rachunkow();
	tablica  = stworzTabliceRachunkow(ilosc);
	wypelnijTabliceRachunkow(tablica,ilosc);
	wypiszTabliceRachunkow(tablica,ilosc);
	printf("Srednia zakupu %lf\n", sredniaZakupu(tablica,ilosc));
	printf("Zakupy dokonane karta %i\n", zakupyDokonaneKarta(tablica,ilosc));
	zapiszTabliceRachunkowDoPliku(tablica,ilosc);
	lista = wczytajPlikDoListy();
	wypiszListeRachunkow(lista);
	printf("Srednia zakupu %lf\n", sredniaZakupuLista(lista));
	printf("Zakupy dokonane karta %i\n",kartaWLiscie(lista));
	zapiszListyRachunkowDoPliku(lista);
	drzewo = wczytajPlikDoDrzewa();
	printf("Wypisuje drzewo\n");
	wypiszDrzewoRachunkow(drzewo);
	glebia = 0;
	xglebokoscDrzewa(drzewo,glebia);
	printf("Glebokosc %i\n",glebia);
	printf("Malejaco\n");
	sortujMalejacoDrzewoDoListy(drzewo,&wynik);
	wypiszListeRachunkow(wynik);
}
void* _new(size_t rozmiar)
{
	void* p;
	p = malloc(rozmiar);
	if(p == NULL)
	{
		printf("Brak pamieci!");
		exit(0);
	}
	return p;
}
FILE* xfopen(char* plik, char* tryb)
{
	FILE *out;
	if ((out = fopen(plik, tryb))== NULL)
	{
		printf("Nie można otworzyć pliku.\n");
		exit(0);
	}
	return out;
}
//Zadanie 1.
Rachunek wypelnijRachunek()
{
	Rachunek nowy;
	printf("Podaj miejsce zakupu: ");
	scanf("%s",((nowy).miejsce));
	//printf("%s\n",(nowy).miejsce);
	printf("\n");
	printf("Podaj rok zakupu: ");
	scanf("%i",&(nowy).rok_zakupu);
	printf("\n");
	printf("Podaj kwote: ");
	scanf("%lf",&(nowy).kwota);
	printf("Gotowka czy karta: ");
	scanf("%i",&(nowy).gotowka_karta);
	printf("\n");
	zapisz_na_dysk(nowy);
	nowy = wczytaj_z_dysu();
	return nowy;
}
int zapisz_na_dysk(Rachunek rachunek)
{
	FILE *out;	
	out = NULL;
	out = xfopen("rachunek.txt", "wt");
	fprintf(out, "%s ", (rachunek).miejsce);
	fprintf(out, "%i ", (rachunek).rok_zakupu); 
	fprintf(out, "%lf ", (rachunek).kwota); 
	fprintf(out, "%i ", (rachunek).gotowka_karta);
	fclose(out); 
	return 0;
}
Rachunek wczytaj_z_dysu()
{
	Rachunek rachunek;
	FILE *out;	
	out = NULL;
	//rachunek = xmalloc(Rachunek,1);
	out = xfopen("rachunek.txt", "rt");
	fscanf(out, "%s", (rachunek).miejsce);
	fscanf(out, "%i", &(rachunek).rok_zakupu); 
	fscanf(out, "%lf",&(rachunek).kwota); 
	fscanf(out, "%i", &(rachunek).gotowka_karta);
	fclose(out); 
	return rachunek;
}
//Zadanie B.
int podaj_ilosc_rachunkow()
{
	int ilosc;
	printf("Podaj ilosc rachunkow.\n");
	scanf("%i",&ilosc);
	return ilosc;
}
Rachunek *stworzTabliceRachunkow(int ilosc)
{
	Rachunek *tablica;
	tablica = xmalloc(Rachunek, ilosc);
	return tablica;
}
int wypelnijTabliceRachunkow(Rachunek *tablica, int ilosc)
{
	int iteracja;
	for (iteracja = 0; iteracja < ilosc; iteracja += 1)
	{
		*(tablica + iteracja) = wypelnijRachunek();
	}
	return 0;
}
int wypiszRachunek(Rachunek rachunek)
{
	printf("############################################\n");
	printf("Miejsce: %s\n", rachunek.miejsce);
	printf("Rok: %i\n",rachunek.rok_zakupu);
	printf("Kwota: %lf\n",rachunek.kwota);
	printf("Gotowka 1 , Karta 0: %i\n",rachunek.gotowka_karta);
	printf("############################################\n");
	return 0;
}
int wypiszTabliceRachunkow(Rachunek *tablica, int ilosc)
{
	int iteracja;
	for (iteracja = 0; iteracja < ilosc; iteracja += 1)
		wypiszRachunek(*(tablica + iteracja));
	return 0;
}
int sumaZakupu(Rachunek *tablica, int ilosc)
{
	int iteracja;
	int suma;
	suma = (*tablica).kwota;
	for (iteracja = 1; iteracja < ilosc; iteracja += 1)
		suma = suma + (*(tablica + iteracja)).kwota;
	return suma;
}
double sredniaZakupu(Rachunek *tablica, int ilosc)
{
	return sumaZakupu(tablica,ilosc)/ilosc;
}
int zakupyDokonaneKarta(Rachunek *tablica, int ilosc)
{
	int iteracja;
	int wynik = 0;
	for (iteracja = 0; iteracja < ilosc; iteracja += 1)
	{
		if ((*(tablica + iteracja)).gotowka_karta == KARTA)
		{
			wynik += 1;
		}
	}
	return wynik;
}
int zapiszTabliceRachunkowDoPliku(Rachunek *tablica, int ilosc)
{

	int  iteracja;
	FILE *out;
	out = xfopen("Tablica.txt", "wt");
	fprintf(out, "%i ", ilosc);
	for (iteracja = 0; iteracja < ilosc; iteracja += 1)
	{
		fprintf(out, "%s ", (*(tablica + iteracja)).miejsce);
		fprintf(out, "%i ", (*(tablica + iteracja)).rok_zakupu); 
		fprintf(out, "%lf ", (*(tablica + iteracja)).kwota); 
		fprintf(out, "%i ", (*(tablica + iteracja)).gotowka_karta);		
	}
	fclose(out);
	return 0;
}
//Czesc C.
Element *stworzElement(Rachunek wartosc, Element *nastepny)
{
	Element *wynik;
	wynik = xmalloc(Element,1);
	(*wynik).wartosc = wartosc;
	(*wynik).nastepny = nastepny;
	return wynik;	
}
Element *dodajElementDoListy(Element *lista, Element *doDodania) //Append
{
	Element *wynik;
	if (lista == NULL)
		return doDodania;
	wynik = lista;
	while((*lista).nastepny != NULL)
		lista = (*lista).nastepny;
	(*lista).nastepny = doDodania;
	return wynik;
}
Element *insertElementDoListy(Element *lista, Element *doDodania) //Insert
{
	if (lista == NULL)
		return doDodania;
	(*doDodania).nastepny = lista;
	return doDodania;
}
Element *wczytajPlikDoListy()
{
	int iteracja, ilosc;
	Element *lista, *rekord;
	Rachunek wartosc;
	FILE *out;
	lista = NULL;
	out = NULL;
	out = xfopen("Tablica.txt", "rt");
	fscanf(out, "%i", &ilosc);
	for (iteracja = 0; iteracja < ilosc; iteracja += 1)
	{
		fscanf(out, "%s", (wartosc).miejsce);
		fscanf(out, "%i", &(wartosc).rok_zakupu); 
		fscanf(out, "%lf",&(wartosc).kwota); 
		fscanf(out, "%i", &(wartosc).gotowka_karta); 
		rekord = stworzElement(wartosc, NULL);
		lista = dodajElementDoListy(lista, rekord);	
	}
	fclose(out);
	return lista;
}
int wypiszListeRachunkow(Element *lista)
{
	while (lista != NULL)
	{
		wypiszRachunek((*lista).wartosc);
		lista = (*lista).nastepny;
	}
	return 0;
}
int policzIleRachunkowWLiscie(Element *lista)
{
	int wynik;
	wynik = 0;
	while (lista != NULL)
	{
		wynik += 1;
		lista = (*lista).nastepny;
	}
	return wynik;
}
int kartaWLiscie(Element *lista)
{
	int wynik;
	wynik = 0;
	while (lista != NULL)
	{
		if ((*(lista)).wartosc.gotowka_karta == KARTA)
		{
			wynik += 1;
		}
		lista = (*lista).nastepny;
	}
	return wynik;
}
double sumaZakupuLista(Element *lista)
{
	int suma;
	suma = (*lista).wartosc.kwota;
	lista = (*lista).nastepny;
	while (lista != NULL)
	{
		suma = suma + (*lista).wartosc.kwota;
		lista = (*lista).nastepny;
	}
	return suma;
}
double sredniaZakupuLista(Element *lista)
{
	return sumaZakupuLista(lista)/policzIleRachunkowWLiscie(lista);
}
int zapiszListyRachunkowDoPliku(Element *lista)
{
	FILE *out = NULL;
	out = xfopen("Lista.txt", "wt");
	fprintf(out, "%i ", policzIleRachunkowWLiscie(lista));
	while (lista != NULL)
	{	
		fprintf(out, "%s ", (*lista).wartosc.miejsce);
		fprintf(out, "%i ", (*lista).wartosc.rok_zakupu); 
		fprintf(out, "%lf ", (*lista).wartosc.kwota); 
		fprintf(out, "%i ", (*lista).wartosc.gotowka_karta);	
		lista = (*lista).nastepny;
	}
	fclose(out);
	return 0;
}
//Czesc D
int wiekszyRachunek(Rachunek pierwszy, Rachunek drugi)
{
	return pierwszy.rok_zakupu > drugi.rok_zakupu ? 1 : 0;
}
Wezel *stworzWezel(Rachunek wartosc, Wezel *lewy, Wezel *prawy)
{
	Wezel *wynik = xmalloc(Wezel,1);
	(*wynik).wartosc = wartosc;
	(*wynik).lewy = lewy;
	(*wynik).prawy = prawy;
	return wynik;	
}
Wezel *dodajWezelDoDrzewa(Wezel *drzewo, Wezel *doDodania)
{
	int a;
	Wezel *wynik = NULL;
	if (drzewo == NULL)
		return doDodania;
	wynik = drzewo;
	a = 1;
	while(a == 1)
	{
		if (wiekszyRachunek((*doDodania).wartosc,(*drzewo).wartosc))
		{
			if ((*drzewo).prawy != NULL)
				drzewo = (*drzewo).prawy;
			else
			{
				(*drzewo).prawy = doDodania;
				a = 0;
			}
		}
		else
		{
			if ((*drzewo).lewy != NULL)
				drzewo = (*drzewo).lewy;
			else
			{
				(*drzewo).lewy = doDodania;
				a = 0;
			}
		}
	}
	return wynik;
}
Wezel *wczytajPlikDoDrzewa()
{
	int iteracja, ilosc;
	Wezel *drzewo, *rekord;
	Rachunek wartosc;
	FILE *out;
	drzewo = NULL;
	out = xfopen("Tablica.txt", "rt");
	fscanf(out, "%i", &ilosc);
	for (iteracja = 0; iteracja < ilosc; iteracja += 1)
	{
		fscanf(out, "%s", (wartosc).miejsce);
		fscanf(out, "%i", &(wartosc).rok_zakupu); 
		fscanf(out, "%lf",&(wartosc).kwota); 
		fscanf(out, "%i", &(wartosc).gotowka_karta);
		rekord = stworzWezel(wartosc, NULL, NULL);
		drzewo = dodajWezelDoDrzewa(drzewo, rekord);
	}
	fclose(out);
	return drzewo;
}
int wypiszDrzewoRachunkow(Wezel *drzewo)
{
	wypiszRachunek((*drzewo).wartosc);
	if ((*drzewo).lewy != NULL)
		wypiszDrzewoRachunkow((*drzewo).lewy);
	if ((*drzewo).prawy != NULL)
		wypiszDrzewoRachunkow((*drzewo).prawy);
	return 0;
}
int glebokoscDrzewa(Wezel *root, int handler, int *glebia)
{
	if(root == NULL)
		return 0; /* puste */
	if((*root).lewy == NULL && root->prawy == NULL){
		handler += 1;
		if(handler > *glebia)
			*glebia = handler;
	}
	else
	{
		handler += 1;
		if ((*root).prawy != NULL)
			glebokoscDrzewa(root->prawy, handler, glebia);
		if ((*root).lewy != NULL )
			glebokoscDrzewa(root->lewy, handler, glebia);
	}
	return 0;	
}
int sortujMalejacoDrzewoDoListy(Wezel *root, Element **lista)
{
	Element *rekord;
	if (root == NULL)
		return 0; /* puste */
	if ((*root).lewy == NULL && root->prawy == NULL)
	{
		rekord = stworzElement((*root).wartosc,NULL);
		*lista = dodajElementDoListy(*lista, rekord);
	}
	else
	{
		if ((*root).prawy != NULL) 
			sortujMalejacoDrzewoDoListy(root->prawy, lista);
		rekord = stworzElement((*root).wartosc,NULL);
		*lista = dodajElementDoListy(*lista, rekord);
		if ((*root).lewy != NULL) 
			sortujMalejacoDrzewoDoListy((*root).lewy, lista);
	}
	return 0;
}
