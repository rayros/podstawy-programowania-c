	/*
	* 
	* My brothers of the sword! 
	* I would rather fight beside you than any army of thousands!
	* Let no man forget how menacing we are! 
	* We are lions! 
	* Do you know what's there, waiting beyond that beach? 
	* Immortality! 
	* Take it! 
	* It's yours!
	*
	*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
	1           0
	|	    |
	1	    1
	|	    |
0 - 1 - 2 - 3 - 1 - 2 - 3 - 0
     	|   |		|
     	0   1  		1
     	    |		|
     	    3 - 1 - 0	0
     	    |
    0 - 1 - 2 - 1 - 0
            |
            0
*/
/*
1	1       1
|	|       |
2 - 3 - 2 - 3 - 2
|       |       |
1       1       1
        |
        0
1 + (1/2)/(2/3) + 34
    
            0
            |
	1   1
	+   |
    1 - 2 - 2 - 1
            |
            0
1 + 2/3/4
1+ 2*(4/3)
*/

#define xmalloc(typ, ilosc) (typ*) _new((ilosc)*sizeof(typ))
#define NIC 0
#define LICZBA 1
#define ULAMEK 2
#define NAWIAS 3
#define MINUS 1
#define PLUS 0
#define ILOCZYN 2
#define BEZ 3
typedef struct element {
	int typ; // 0 = NIC , 1 = LICZBA, 2 = ULAMEK, 3 = NAWIAS
	int znak; // 0 = PLUS, 1 = MINUS, 2 = ILOCZYN, 3 = BEZ
	struct liczba {
		int wartosc;
	} liczba;
	struct ulamek {
		struct element *licznik;
		struct element *mianownik;
	} ulamek;
	struct nawias {
		struct element *zawartosc;
	} nawias;
	struct element *nastepny;	
	struct element *poprzedni;
} Element;
extern void* _new(size_t rozmiar); 
int nwd(int a, int b);
Element *stworzElement(Element *nastepny);
char *substring(int start, int stop, const char *text);
Element* getIt(int poczatek, char *string);
Element *dodajElementDoListy(Element *lista, Element *doDodania);
int main ()
{
	Element *lista;
	lista = NULL;
	char buffer[BUFSIZ];
	fgets(buffer , BUFSIZ+1 , stdin);
	lista = getIt(0,buffer);
	return 0;
}
Element* getIt(int poczatek, char *string)
{
	Element *lista;
	Element *nowy;
	nowy = stworzElement(NULL);
	/*********************\
	|** YES MY MASTER!! **|
	\*********************/
	int i = 0;
	int init = 0;
	int integer = 0;
	int start = 0;
	int end = 0; 
	while (string[i] != '\0')
	{
		for (int a = 0; a < 9; a += 1)
		{	
			if((int)string[i]==a+48){
				integer = 1;
				a=9;
			}else{
				integer = 0;
			}
		}
		if (integer == 1)
		{
			if (init == 0)
			{
				init = 1;
				start = i;
			}
			end++;
		}else{
			if (init == 1)
			{
				
				init = 0;
				char *sub = substring( start, end+start, string);
				int a = 0;
				sscanf(sub,"%i",&a);
				if((*nowy).znak == BEZ)
					(*nowy).znak = PLUS;
				(*nowy).liczba.wartosc = a;
				lista = dodajElementDoListy(lista,nowy);
				free(sub);
				end = 0;
			}
		}
		if (string[i] == '+' || string[i] == '-' || string[i] == '*' || string[i] == '/' || string[i] == '(' || string[i] == ')')
		{
			switch(string[i]){
				case '+':(*nowy).znak = PLUS; break;
				case '-':(*nowy).znak = MINUS; break;
				case '*':(*nowy).znak = ILOCZYN; break;
				default: (*nowy).znak = BEZ;
			}				
		}		
		i++;
	}
	return nowy;
}
char *substring(int start, int stop, const char *text)
{
	char *dst=(char*)malloc((stop-start+1)*sizeof(char));
	snprintf(dst,(stop-start+1),"%.*s", stop - start, &text[start]);
	return dst;
}
int nwd(int a, int b)
{
	return b == 0 ? a : nwd(b, a%b);
}
void* _new(size_t rozmiar)
{
	void* p;
	p = malloc(rozmiar);
	if(p == NULL)
	{
		printf("Brak pamieci!");
		exit(0);
	}
	return p;
}
Element *stworzElement(Element *nastepny)
{
	Element *wynik;
	wynik = xmalloc(Element,1);
	(*wynik).nastepny = nastepny;
	(*wynik).znak = BEZ;
	return wynik;	
}
Element *dodajElementDoListy(Element *lista, Element *doDodania) //Append
{
	Element *wynik;
	if (lista == NULL)
		return doDodania;
	wynik = lista;
	while((*lista).nastepny != NULL)
		lista = (*lista).nastepny;
	(*lista).nastepny = doDodania;
	return wynik;
}
