#include"stdio.h"
#include"stdlib.h"
#include"math.h"
#include"string.h"
#define xmalloc(typ, ilosc) (typ*) _new((ilosc)*sizeof(typ))
#define NO_FILE 1;
struct film {
	int id;
	char tytul[50];
	char rezyser[50];
	int rok;
	float czasTrwania;
};
struct element{
	struct film dana;
	struct element *nastepny;
};
typedef struct wezel {
	struct film wartosc;
	struct wezel *lewy;
	struct wezel *prawy;
} Wezel;
struct film wypelnijRekord();
struct element * wstaw(struct element *l, struct film d);
void wypisz(struct element *l);
struct element * doczep(struct element *l, struct film d);
int ileElementow(struct element *l);
struct element * usun(struct element *l);
int zapiszNaDysk(struct element *l, char * listaf);
int ostatnieId(struct element *lista);
struct element * odczytajZDysku(char * listaf, int *error);
struct element* usunElement(struct element *lista, int id);
Wezel *stworzDrzewo(struct element *lista);
Wezel *stworzWezel(struct film wartosc, Wezel *lewy, Wezel *prawy);
Wezel *dodajWezelDoDrzewa(Wezel *drzewo, Wezel *doDodania);
int sortujMalejacoDrzewoDoListy(Wezel *root, struct element **lista);
int sortujRosnacoDrzewoDoListy(Wezel *root, struct element **lista);
struct element* wyszukaj(struct element *lista, char* nazwa);
void* _new(size_t rozmiar)
{
	void* p;
	p = malloc(rozmiar);
	if(p == NULL)
	{
		printf("Brak pamieci!");
		exit(0);
	}
	return p;
}
int main()
{
	struct element *lista = NULL;
	int i, ile, error, status;
	struct film p;
	printf("%i",strcmp("Bdam","m"));
	lista = odczytajZDysku("lista.txt",&error);
	
	printf("Podaj numer opcji:\n\n");
	printf("1 - Wstaw rekord do bazy\n");
	printf("2 - Zapisz zmiany\n");
	printf("3 - Sortowanie rekordow\n");
	printf("4 - Usuwanie rekordu\n");
	printf("5 - Usuwanie bazy\n");
	printf("6 - Odczyt bazy\n");
	printf("7 - Wyszukaj\n");
	/*printf("5 - Wyszukiwanie rekordow\n");
	printf("8 - Kasowanie bazy\n");
	printf("Zakonczenie programu (Ctrl + Z + Enter)\n");
	*/
	int numerId,a;
	char nazwa[50];
	struct element *wyszuka;
	while(scanf("%i", &a)){
		switch(a){
			case 1 : {
				printf("Wstaw rekord do bazy\n\n");
				printf("Podaj liczbe rekordow do wstawienia:");
				scanf("%i", &ile);
				for(i = 0; i < ile; i++){
					p = wypelnijRekord();
					p.id = ostatnieId(lista)+1;
					lista = doczep(lista,p);
				}
			}
			break;
			case 2 : {
				printf("Zapisz rekord na dysk\n\n");
				printf("Zapisuje baze filmow do pliku:\n");
				printf("--------------------------------------\n");
				status = zapiszNaDysk(lista, "lista.txt");
				if(status == 1)
					printf("Zapis OK\n");
				else
					printf("Zapis sie nie powiodl\n");
				printf("\n--------------------------------------\n");
			}
			break;
			case 3 : {
				printf("Sortowanie rekordow: A - Z\n\n");
				Wezel *drzewo = stworzDrzewo(lista);
				struct element *nowy = NULL;
				sortujRosnacoDrzewoDoListy(drzewo, &nowy); // A - Z
				wypisz(nowy);
			}
			break;
			case 4 : {
				printf("Usuwanie rekordu\n\n");
				printf("Podaj numer id: ");
				scanf("%i",&numerId);
				lista = usunElement(lista,numerId);
				printf("Usunieto.\n");
			}
			break;
			case 5: {
				printf("Usuwanie bazy\n\n");
				lista = usun(lista);
				status = zapiszNaDysk(lista, "lista.txt");
				if(status == 1)
					printf("Zapis OK\n");
				else
					printf("Zapis sie nie powiodl\n");
			}
			break;
			case 6: {
				printf("Wypisujemy bazę: \n\n");
				wypisz(lista);
			}
			break;
			case 7: {
				printf("Wyszukaj: ");
				scanf("%s",nazwa);
				if ((wyszuka = wyszukaj(lista,nazwa)) != NULL)
				{
					wypisz(wyszuka);
				}else{
					printf("Nie znaleziono\n");
				}
			}
		}
	}
	//system("PAUSE");
}
struct element* wyszukaj(struct element *lista, char* nazwa)
{
	while (lista != NULL)
	{
		if (strcmp(nazwa, (*lista).dana.tytul) == 0)
		{
			printf("MAM!\n");
			(*lista).nastepny = NULL;
			return lista;
		}
		lista = (*lista).nastepny;
	}
	return NULL;
}
struct film wypelnijRekord()
{
	struct film nowy;
	printf("Podaj dane do filmu:\n - tytul     : ");
	scanf(" %[-' A-Za-z0-9]s",nowy.tytul);
	printf(" - rezyser : ");
	scanf("%s", nowy.rezyser);
	printf(" - rok produkcji  : ");
	scanf("%i", &nowy.rok);
	printf(" - czas trwania filmu   : ");
	scanf("%f", &nowy.czasTrwania);
	return nowy;	
}
int ostatnieId(struct element *lista)
{
	int ostatnie = -1;
	while(lista != NULL)
	{
		if((*lista).dana.id > ostatnie){
			ostatnie = (*lista).dana.id;
		}
		lista = (*lista).nastepny;
	}
	return ostatnie;
}
struct element * wstaw(struct element *head, struct film d){
	struct element * nowy;
	nowy = (struct element *) malloc(sizeof(struct element));
	if(nowy == NULL)
	{
		printf("Not enough memory.\n");
		exit( -1);
	}
	(*nowy).nastepny = head;
	(*nowy).dana = d;
	return nowy;
}
void wypisz(struct element *h){
	struct element * wsk;
	wsk = h;
	while(wsk != NULL){
		printf(" - id    : %i\n", (*wsk).dana.id);
		printf(" - tytul    : %s\n", (*wsk).dana.tytul);
		printf(" - rezyser  : %s\n", (*wsk).dana.rezyser);
		printf(" - rok produkcji : %i\n", (*wsk).dana.rok);
		printf(" - czas trwania filmu : %f\n", (*wsk).dana.czasTrwania);
		printf("\n");
		wsk = (*wsk).nastepny;
	}
}
struct element* usunElement(struct element *lista, int id)
{
	struct element *nowy;
	nowy = NULL;
	while (lista != NULL)
	{
		if((*lista).dana.id != id)
		{
			nowy = doczep(nowy,(*lista).dana);
		}
		lista = (*lista).nastepny;
	}
	return nowy;
}
struct element * doczep(struct element *head, struct film d){
	if(head == NULL){
		return wstaw(head, d);
	}
	// Znajdz ostatni element
	struct element * wsk;
	wsk = head;
	while((*wsk).nastepny != NULL){
		wsk = (*wsk).nastepny;
	}
	// Wstaw za ostatnim elementem
	struct element * nowy;
	nowy = (struct element *) malloc(sizeof(struct element));
	if(nowy == NULL){
		printf("Not enough memory.\n");
		exit( -1);
	}
	(*nowy).nastepny = NULL;
	(*nowy).dana = d;
	(*wsk).nastepny = nowy;
	return head;
}
int ileElementow(struct element *h){
	struct element * wsk;
	int cnt = 0;
	wsk = h;
	while(wsk){
		wsk = (*wsk).nastepny;
		cnt++;
	}
	return cnt;
}
struct element* usun(struct element *l){
	struct element * tmp = l;
	while( l != NULL){
		tmp = l;
		l = (*l).nastepny;
		free(tmp);
	}
	return NULL;
}
int zapiszNaDysk(struct element *wsk, char * listaf){
	FILE *plik;
	int rozmiar = ileElementow(wsk);
	if ((plik = fopen(listaf, "wt")) == NULL){
		printf("Cannot open output file.\n");
		return -1;
	}
	fprintf(plik, "%i\n", rozmiar);
	while(wsk != NULL){
		fprintf(plik, "%i\n", (*wsk).dana.id);
		fprintf(plik, "%s\n", (*wsk).dana.tytul);
		fprintf(plik, "%s\n", (*wsk).dana.rezyser);
		fprintf(plik, "%i\n", (*wsk).dana.rok);
		fprintf(plik, "%f\n", (*wsk).dana.czasTrwania);
		wsk = (*wsk).nastepny;
	}
	fclose(plik);
	return 1;
}
struct element * odczytajZDysku(char * listaf, int *error)
{
	FILE *plik;
	int rozmiar, i;
	struct film p;
	struct element *lista = NULL;
	if ((plik = fopen(listaf, "rt")) == NULL)
	{
		printf("Cannot open output file.\n");
		*error = NO_FILE;
		return NULL;
	}
	fscanf(plik, "%i", &rozmiar);
	for(i = 0; i < rozmiar; i++)
	{
		fscanf(plik, "%i", &p.id);
		fscanf(plik," %[-' A-Za-z0-9]s", p.tytul);
		fscanf(plik, "%s", p.rezyser);
		fscanf(plik, "%i", &p.rok);
		fscanf(plik, "%f", &p.czasTrwania);
		lista = doczep(lista,p);
	}
	fclose(plik);
	return lista;
}
Wezel *stworzDrzewo(struct element *lista)
{
	Wezel *rekord = NULL,*drzewo = NULL;
	while (lista != NULL)
	{
		rekord = stworzWezel((*lista).dana,NULL,NULL);
		drzewo = dodajWezelDoDrzewa(drzewo, rekord);
		lista = (*lista).nastepny;
	}
	return drzewo;
}
Wezel *stworzWezel(struct film wartosc, Wezel *lewy, Wezel *prawy)
{
	Wezel *wynik = xmalloc(Wezel,1);
	(*wynik).wartosc = wartosc;
	(*wynik).lewy = lewy;
	(*wynik).prawy = prawy;
	return wynik;	
}
Wezel *dodajWezelDoDrzewa(Wezel *drzewo, Wezel *doDodania)
{
	int a;
	Wezel *wynik = NULL;
	if (drzewo == NULL)
		return doDodania;
	wynik = drzewo;
	a = 1;
	while(a == 1)
	{
		if (strcmp((*doDodania).wartosc.tytul,(*drzewo).wartosc.tytul) >= 0)
		{
			if ((*drzewo).prawy != NULL)
				drzewo = (*drzewo).prawy;
			else
			{
				(*drzewo).prawy = doDodania;
				a = 0;
			}
		}
		else
		{
			if ((*drzewo).lewy != NULL)
				drzewo = (*drzewo).lewy;
			else
			{
				(*drzewo).lewy = doDodania;
				a = 0;
			}
		}
	}
	return wynik;
}
int sortujMalejacoDrzewoDoListy(Wezel *root, struct element **lista)
{
	if (root == NULL)
		return 0; /* puste */
	if ((*root).lewy == NULL && root->prawy == NULL)
	{
		*lista = doczep(*lista, (*root).wartosc);
	}
	else
	{
		if ((*root).prawy != NULL) 
			sortujMalejacoDrzewoDoListy(root->prawy, lista);
		*lista = doczep(*lista, (*root).wartosc);
		if ((*root).lewy != NULL) 
			sortujMalejacoDrzewoDoListy((*root).lewy, lista);
	}
	return 0;
}
int sortujRosnacoDrzewoDoListy(Wezel *root, struct element **lista)
{
	if (root == NULL)
		return 0; /* puste */
	if ((*root).lewy == NULL && root->prawy == NULL)
	{
		*lista = doczep(*lista, (*root).wartosc);
	}
	else
	{
		if ((*root).lewy != NULL) 
			sortujRosnacoDrzewoDoListy((*root).lewy, lista);
		*lista = doczep(*lista, (*root).wartosc);
		if ((*root).prawy != NULL) 
			sortujRosnacoDrzewoDoListy(root->prawy, lista);
	}
	return 0;
}
