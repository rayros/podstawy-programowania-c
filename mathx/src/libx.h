#ifndef H_XLIB
#define H_XLIB
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#define xmalloc(typ, ilosc) (typ*) _new((ilosc)*sizeof(typ))
extern void* _new(size_t rozmiar);
#endif
