#include "mathx.h"
Fraction newFraction(int sign, Total total, Total counter, Total denominator)
{
	Fraction x;
	x.sign = sign;
	x.total = total;
	x.counter = counter;
	x.denominator = denominator;
	return x;
}
Total NWW(Total a, Total b)
{
	int nww = _NWW(a.value,b.value);
	return newTotal(PLUS,nww);
}
int _NWW(int a, int b)
{
	return a*b/_NWD(a,b);
}
int _NWD(int a, int b)
{
	return b == 0 ? a : _NWD(b, a % b);
}
Total NWD(Total a, Total b)
{
	int nwd = _NWD(a.value, b.value);
	return newTotal(PLUS, nwd);
}
Total newTotal(int sign, int value)
{
	Total a;
	a.sign = sign;
	a.value = value;
	return a;
}
Total addTotal(Total a, Total b){
	Total x;
	if (a.sign == b.sign)
	{
		x.sign = a.sign;
		x.value = a.value + b.value;
	}
	else
	{
		if (a.value >= b.value)
		{
			x.sign = a.sign;
			x.value = a.value - b.value;
		}
		else
		{
			x.sign = b.sign;
			x.value = b.value - a.value;
		}
	}
	return x;
}
Total subTotal(Total a, Total b)
{
	Total x;
	if (a.sign == b.sign)
	{
		if (a.value >= b.value)
		{
			x.sign = a.sign;
			x.value = a.value - b.value;
		}
		else
		{
			if (a.sign == PLUS)
			{
				x.sign = MINUS;
			}
			else
			{
				x.sign = PLUS;
			}
			x.value = b.value - a.value;
		}
	}
	else
	{
		x.sign = a.sign;
		x.value = a.value + b.value;
	}
	return x;
}
Point2D createPoint2D(int x, int y){
	Point2D a;
	a.x = x;
	a.y = y;
	return a;
}
Vector2D createVector2D(Point2D a,Point2D b){
	Vector2D v;
	v.a = a;
	v.b = b;
	return v;
}
Point2D vectorCoordinates(Vector2D v){
	Point2D p;
	p.x = v.b.x - v.a.x;
	p.y = v.b.y - v.a.y;
	return p;
}
bool isSqrt(int a){
	int b = (int) sqrt(a);
	if(a == b*b){ return true;}
	else{ return false;}
}
bool isNatural(int a){
	if (a >= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool isNaturalPlus(int a){
	if (a > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
