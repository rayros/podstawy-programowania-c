#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>


typedef struct uint64_tArray{
	uint64_t *array;
	size_t size;
} uint64_tArray;

int uint64_tArrayInit( uint64_tArray **array);
int uint64_tArrayAdd( uint64_tArray **array, uint64_t value );
uint64_t uint64_tArrayGet( uint64_tArray **array, size_t index );
int uint64_tArrayFree( uint64_tArray **array );
int uint64_tArrayJoin( uint64_tArray **arrayA, uint64_tArray **arrayB, uint64_tArray **arrayC);
int uint64_tArrayCopy( uint64_tArray **copy, uint64_tArray *toCopy );

int main ()
{
	uint64_tArray *a;
	uint64_tArrayInit(&a);
	uint64_tArrayAdd(&a,121);
	size_t liczba = a->size;
	printf( "%i\n",liczba );
		
	return 0;
}
int uint64_tArrayGetSize(uint64_tArray **array)
{

	return 0;
}
int uint64_tArrayInit( uint64_tArray **array)
{
	*array = NULL;
	return 0;
}

int uint64_tArrayAdd( uint64_tArray **array, uint64_t value )
{
	if ( *array == NULL )
	{
		*array = ( uint64_tArray* ) malloc( sizeof( uint64_tArray ) );
		(*array)->array = ( uint64_t* ) malloc( sizeof( uint64_t ) ); 
		*( (*array)->array ) = value;
		(*array)->size = 0;
	}
	else
	{
		(*array)->size += 1;
		uint64_t *b = ( uint64_t* ) realloc( (*array)->array, ( (*array)->size + 1 ) * sizeof( *b ) );
		if ( b == NULL )
		{
			printf( "Za mało pamięci. :(" );
			exit(0);
		}
		(*array)->array = b;
		*( (*array)->array + (*array)->size ) = value;
	}
	return 0;
}

uint64_t uint64_tArrayGet( uint64_tArray **array, size_t index )
{
	if( *array != NULL )
	{
		return *((*array)->array + index);
	}
	else
	{
		return -1;
	}
	
}

int uint64_tArrayFree( uint64_tArray **array )
{
	if ( *array != NULL )
	{
		free( (*array)->array );
		free( (*array) );
	}
	return 0;
}
int uint64_tArrayJoin( uint64_tArray **arrayA, uint64_tArray **arrayB, uint64_tArray **arrayC)
{
	uint64_tArrayFree(arrayC);
	*arrayC = malloc(sizeof(uint64_tArray));
	(*arrayC)->array = malloc( (((*arrayA)->size+1)+((*arrayB)->size+1))*sizeof(uint64_t));
	memcpy( (*arrayC)->array, (*arrayA)->array, ( (*arrayA)->size + 1 ) * sizeof( uint64_t ) );
	memcpy( (*arrayC)->array + ((*arrayA)->size + 1), (*arrayB)->array, ( (*arrayB)->size + 1 ) * sizeof( uint64_t ) );
	(*arrayC)->size = (*arrayA)->size + (*arrayB)->size + 1;
	return 0;
}
int uint64_tArrayCopy( uint64_tArray **copy, uint64_tArray *toCopy )
{
	uint64_tArrayFree( copy );
	*copy = malloc( sizeof( uint64_tArray ) );
	(*copy)->array = malloc( ( toCopy->size + 1 ) * sizeof( uint64_t ) );
	memcpy( (*copy)->array, toCopy->array, ( toCopy->size + 1 ) * sizeof( uint64_t ) );
	(*copy)->size = toCopy->size;
	return 0;
}

