#include<stdio.h>
#include<stdlib.h>
main(){
   printf("Moja wizytowka\n\n");
   printf("+---------------------------------------------+\n");
   printf("| Autor : _____ ________                      |\n");
   printf("| Grupa : _____ ________                      |\n");
   printf("| e-mail: _____ ________                      |\n");
   printf("+---------------------------------------------+\n");
   printf("\n");

   int rokUrodzenia = 1986;
   float wzrost = 176.4;
   char plec = 'K';

   printf("Podaj rok urodzenia : ");
   fflush(stdin);
   scanf("%i", &rokUrodzenia);

   printf("Podaj wzrost        : ");
   fflush(stdin);
   scanf("%f", &wzrost);

   printf("Podaj plec          : ");
   fflush(stdin);
   scanf("%c", &plec);

   printf("\n");

   printf("Rok urodzenia     : %i \n", rokUrodzenia);
   printf("Wzrost delikwenta : %f \n", wzrost);
   printf("Plec delikwenta   : %c \n", plec);

   printf("\n\n");

   int aktualnyRok;
   printf("Podaj aktualny rok: ");
   fflush(stdin);
   scanf("%i", &aktualnyRok);

   int wiek;
   wiek = aktualnyRok - rokUrodzenia;

   printf("\n\n");
   printf("Urodziles sie w %i roku, wiec masz %i lat\n", rokUrodzenia, wiek);

   printf("\n\n");
   system("PAUSE");
}

