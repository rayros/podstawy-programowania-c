/*
	Fibo
	Liczenie ciągu Fibonacciego.
	Wersja: 2  // Usunięto wycieki pamięci.

	Paweł Łaski (c) 2013

	gcc -o fibo fibo.c -pedantic -Wall -Wextra -std=c99

	Udostępnione tylko do celów edukacyjnych.
	
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

typedef char Digit;
typedef char MiniInt;

typedef struct {
	Digit digit;
} QueueRecord;

typedef struct List{
	QueueRecord *record;
	struct List *previous;
	struct List *next;
} List;

typedef struct {
	List *first;
	List *last;
} Queue;

typedef Queue Big;

int queueInit( Queue *queue );
int queueAddLast( Queue *queue, Digit digit );
int queueAddFirst( Queue *queue, Digit digit );
QueueRecord queueGet( Queue *queue );
int queueRemove( Queue *queue );
int queueRemoveAll( Queue *queue );
int queueDisplay( Queue *queue );

int invertBig( Big *big );
int addLastDigitToBig( Big *big, Digit digit);
int addFirstDigitToBig( Big *big, Digit digit);
int isDigit( Digit digit );
int freeBig(Big *big);
int printInvertBig( Big *big );
int printBig( Big *big );
int initBig(Big *big);
int additionBigs(Big *a, Big *b, Big *result);
int copyToBig(Big *a, Big *b);

int main()
{
	clock_t start,finish;
	
	Big a, b, c;
	int in;
	printf( "\tDo którego wyrazu ciągu?\n" );
	printf( "\tNumer wyrazu?: " );
	scanf( "%i", &in );
	start = clock();
	initBig( &a );
	initBig( &b );
	initBig( &c );
	addFirstDigitToBig( &a, 1);
	printInvertBig(&a);
	printf( "\n" );
	addFirstDigitToBig( &b, 1);
	printInvertBig(&b);
	printf( "\n" );
	additionBigs( &a, &b, &c);
	printInvertBig(&c);
	printf( "\n" );
	for (int i = 0; i < in-3; i += 1)
	{
		copyToBig(&a, &b);
		copyToBig(&b, &c);
		additionBigs( &a, &b, &c );
		printInvertBig(&c);
		printf( "\n" );
	}
	//printInvertBig(&c);
	printf( "\n" );
	freeBig( &a );
	freeBig( &b );
	freeBig( &c );
	finish = clock();
	double duration = (double)(finish-start) / CLOCKS_PER_SEC;
	printf( "%f",duration );
	printf( "\n\tKONIEC!\n" );
	getchar();
	return 0;
}
int copyToBig(Big *copy, Big *toCopy )
{
	freeBig(copy);
	List *list = toCopy->last;
	while( list )
	{
		addFirstDigitToBig( copy , list->record->digit);
		list = list->previous;
	}
	return 0;
}
int additionBigs(Big *a, Big *b, Big *result)
{
	freeBig(result);
	initBig(result);
	Digit temp = 0;
	MiniInt sum;
	List *big1 = a->first;
	List *big2 = b->first;
	while (big1 != NULL && big2 != NULL)
	{
		sum = big1->record->digit + big2->record->digit + temp;
		#ifdef DEBUG
			printf( "%i + %i + %i = %i \n", big1->record->digit, big2->record->digit, temp, sum );
		#endif
		temp = 0;
		if (isDigit(sum) == FALSE)
		{
			temp = 1;
			sum -=10;
			addLastDigitToBig( result , sum);
		}else
		{
			addLastDigitToBig( result , sum);
		}
		big1 = big1->next;
		big2 = big2->next;
	}
	while (big1 != NULL)
	{
		sum = big1->record->digit + temp;
		#ifdef DEBUG
			printf( "%i + %i = %i \n", big1->record->digit, temp, sum );
		#endif
		temp = 0;
		if (isDigit(sum) == FALSE)
		{
			temp = 1;
			sum -=10;
			addLastDigitToBig( result , sum);
		}else
		{
			addLastDigitToBig( result , sum);
		}
		big1 = big1->next;
	}
	while (big2 != NULL)
	{
		sum = big2->record->digit + temp;
		#ifdef DEBUG
			printf( "%i + %i = %i \n", big2->record->digit, temp, sum );
		#endif
		temp = 0;
		if (isDigit(sum) == FALSE)
		{
			temp = 1;
			sum -=10;
			addLastDigitToBig( result , sum);
		}else
		{
			addLastDigitToBig( result , sum);
		}
		big2 = big2->next;
	}
	if (temp == 1)
	{
		addLastDigitToBig( result,temp);
	}
	return 0;
}
int initBig(Big *big)
{
	queueInit( big );
	return 0;
}
int freeBig(Big *big)
{
	queueRemoveAll(	big );
	return 0;
}
int invertBig(Big *big){
	Big result;
	queueInit( &result );
	List *a = big->first;
	while( a )
	{
		addFirstDigitToBig(&result,a->record->digit);
		a = a->next;
	}
	freeBig( big );
	*big = result;
	return 0;
}
int addFirstDigitToBig( Big *big, Digit digit){
	if (isDigit(digit) == FALSE)
	{
		printf( "addFirstDigitToBig: %i is no Digit.\n",digit );
		return -1;
	}
	queueAddFirst(big, digit);
	return 0;
}
int addLastDigitToBig( Big *big, Digit digit)
{
	if (isDigit(digit) == FALSE)
	{
		printf( "addFirstBig: %i is no Digit.\n",digit );
		return -1;
	}
	queueAddLast(big, digit);
	return 0;
}
int queueInit( Queue *queue )
{
	#ifdef DEBUG
		printf( "# <<<< Init Queue >>>>\n" );
	#endif
	queue->first = NULL;
	queue->last = NULL;
	return 0;
}
int queueAddFirst( Queue *queue, Digit digit )
{
	#ifdef DEBUG
		printf( "# queueAddLast: Adding new QueueRecord: digit - %i\n" ,digit );
	#endif
	QueueRecord *newRecord = ( QueueRecord* ) malloc( sizeof( QueueRecord ) );
	newRecord->digit = digit;
	List *newList = ( List* ) malloc( sizeof( List ) );
	newList->record = newRecord;
	newList->next = NULL;
	newList->previous = NULL;
	if( queue->first == NULL && queue->last == NULL )
	{
		queue->first = newList;
		queue->last = newList;
	}
	else
	{
		newList->next = queue->first;
		queue->first->previous = newList;
		queue->first = newList;
	}
	return 0;
}
int queueAddLast( Queue *queue, Digit digit )
{
	#ifdef DEBUG
		printf( "# queueAddLast: Adding new QueueRecord: digit - %i\n", digit );
	#endif
	QueueRecord *newRecord = ( QueueRecord* ) malloc( sizeof( QueueRecord ) );
	newRecord->digit = digit;
	List *newList = ( List* ) malloc( sizeof( List ) );
	newList->record = newRecord;
	newList->next = NULL;
	newList->previous = NULL;
	if( queue->first == NULL && queue->last == NULL )
	{
		queue->first = newList;
		queue->last = newList;
	}
	else
	{
		queue->last->next = newList;
		queue->last->next->previous = queue->last;
		queue->last = queue->last->next;
	}
	return 0;
}
QueueRecord queueGet( Queue *queue )
{
	#ifdef DEBUG
		printf( "# queueGet: Returning record.\n" );
	#endif
	return *( queue->first->record );
}
int queueRemove( Queue *queue )
{
	#ifdef DEBUG
		printf( "# queueRemove: Removing record.\n" );
	#endif
	if( queue->first == NULL && queue->last == NULL )
	{
		return -1;
	}
	else if( queue->first->next == NULL )
	{
		free( queue->first->record );
		free( queue->first );
		queue->first = NULL;
		queue->last = NULL;
	}
	else
	{
		List *a = queue->first;
		queue->first = queue->first->next;
		free( a->record );
		free( a );
	}
	return 0;
}
int queueRemoveAll( Queue *queue )
{
	#ifdef DEBUG
		printf( "# queueRemoveAll: Removing records.\n" );
	#endif
	while( queueRemove( queue ) == 0 );
	return 0;
}
int queueDisplay( Queue *queue )
{
	#ifdef DEBUG
		printf( "# queueDisplay: Printing records.\n" );
	#endif
	List *a = queue->first;
	while( a )
	{
		printf( "# queueDisplay: Digit - %i\n", a->record->digit);
		a = a->next;
	}
	return 0;
}
int printBig( Big *big )
{
	#ifdef DEBUG
		printf( "# queueDisplay: Printing records.\n" );
	#endif
	List *a = big->first;
	while( a )
	{
		printf( "%i", a->record->digit);
		a = a->next;
	}
	return 0;
}
int printInvertBig( Big *big )
{
	#ifdef DEBUG
		printf( "# queueDisplay: Printing records.\n" );
	#endif
	List *a = big->last;
	while( a )
	{
		printf( "%i", a->record->digit);
		a = a->previous;
	}
	return 0;
}
int isDigit(Digit digit){
	if(digit > 9){
		#ifdef DEBUG
			printf( "# isDigit: %i is no Digit.\n" , digit);
		#endif
		return FALSE;
	}else{
		return TRUE;
	}
}
