#include <stdio.h>
#include <math.h>
#include <time.h>

#define SIZE 30

int LosujTablice(int tablica[], int rozmiar);
int WypiszTablice(int tablica[], int rozmiar);

int main(void){
        srand( (unsigned)time(NULL) );
        int tablica[SIZE];
        LosujTablice(tablica, SIZE);
        WypiszTablice(tablica, SIZE);
        return 1;
}

int LosujTablice(int tablica[], int rozmiar)
{
        int i;
        for( i = 0; i < rozmiar; i++ )
        {
             tablica[i] = rand()%10; // od 0 do 9
        }
        return 0;
}

int WypiszTablice(int tablica[], int rozmiar)
{
        int i;
        if(rozmiar <= 20)
        {
                for(i = 0; i < rozmiar; i++)
                {
                        printf("%i ", tablica[i]);
                }
        }
        else 
        {
                for(i = 0; i < 10; i++)
                {
                        printf("%i ", tablica[i]);
                }
                printf("... ");
                for(i = (rozmiar - 10); i < rozmiar; i++)
                {
                        printf("%i ", tablica[i]);
                }
        }
        return 0;
}

